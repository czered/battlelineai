package de.zoneofcontrol.battleline.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import de.zoneofcontrol.battleline.game.MainGame;

public class DesktopLauncher {

    private final static int RESOLUTION_X = 1600;
    private final static int RESOLUTION_Y = 900;

    public static void main(String[] arg) {
        LwjglApplicationConfiguration cfg = new LwjglApplicationConfiguration();
        cfg.title = "Title";
        cfg.height = RESOLUTION_Y;
        cfg.width = RESOLUTION_X;
//        new LwjglApplication(new MainGame(), cfg);
    }
}
