package de.zoneofcontrol.battleline.resources;

import org.deeplearning4j.nn.weights.WeightInit;
import org.nd4j.linalg.activations.Activation;
import org.nd4j.linalg.lossfunctions.LossFunctions;

public final class Constants {
    private Constants() {
        // restrict instantiation
    }

    // Game constants
    public final static int COLORS = 6;
    public final static int CARD_NUMBERS = 10;
    public final static boolean FIXED_DECK = false;
    public final static int FLAGS = 9;
    public static final int CONSECUTIVES_FLAGS_NEED = 3;
    public static final int NUM_FLAGS_TO_WIN = 5;
    public final static int HANDSIZE = 7;
    public final static int HANDSIZE_TRAINING = 15;
    public static final int BITS_PER_CARDS = 16;
    public static final int CARDS_PER_FLAG = 6;
    public final static int DECK_SIZE = CARD_NUMBERS * COLORS;

    //Input/Output configuration
    public static final int DELTA_MAX_SCORE = 1;
    public static final int WILL_CARD_WIN_FLAG =0;
    public static final int WILL_CARD_WIN_GAME =0;
    public static final int HOW_MANY_SAME_COLORS_IN_HAND =1;
    public static final int HOW_MANY_CONS_NUMBERS_IN_HAND=1;
    public static final int HOW_MANY_SAME_NUMBERS_IN_HAND=1;
    public static final int HOW_MANY_SAME_COLORS_IN_DECK =1;
    public static final int HOW_MANY_CONS_NUMBERS_IN_DECK=1;
    public static final int HOW_MANY_SAME_NUMBERS_IN_DECK=1;
    public static final int NUMBER_INPUTS = DELTA_MAX_SCORE+WILL_CARD_WIN_FLAG+WILL_CARD_WIN_GAME+HOW_MANY_SAME_COLORS_IN_HAND+HOW_MANY_CONS_NUMBERS_IN_HAND+HOW_MANY_SAME_NUMBERS_IN_HAND+HOW_MANY_SAME_COLORS_IN_DECK+HOW_MANY_CONS_NUMBERS_IN_DECK+HOW_MANY_SAME_NUMBERS_IN_DECK;
    public static final int OUTPUTS = 2;

    // Trainer Constants
    public static int NUM_EPOCHS = 20000;
    public static int GAMES_PER_EPOCHS = 300;
    public static int MAX_SIZE = 2000;
    public static int BATCH_SIZE = 8;
    public static int TOTALTRAININGGAMES = NUM_EPOCHS * GAMES_PER_EPOCHS;
    public static final int EVALUATION_SIZE = 100;
    public static final int EVALUATION_INTERVALL = 1;
    public static final int AGENT_SYNC = EVALUATION_INTERVALL;
    public static final float WIN_PERC_NEEDED = 0.63f;
    public static final int CONS_WINS_NEEDED = 1;
    public static final float PREDICTION_START_PERC = 0.1f;

    public static final double GAME_WON_REWARD = 100;
    public static final double GAME_LOST_REWARD = 0;
    public static final double FLAG_WON_REWARD = 10;
    public static final double FLAG_LOST_REWARD = 0;

    public static final double ZERO = -0.2201988;
    public static final double ONE = 4.5360950;

    // Network constants
    public static double EPSILON_DECAY = 0.99999;
    public static double MIN_EPSILON = 0.15;
    public static double LEARNING_RATE = 0.001;
    public static int HIDDEN_NODES = 20;
    public static int NUM_HIDDEN_LAYERS = 2;
    public final static WeightInit INITIALWEIGHT = WeightInit.ONES;
    public final static Activation INNERACTIVATION = Activation.LEAKYRELU;
    public final static Activation FINALACTIVATION = Activation.LEAKYRELU;
    public final static LossFunctions.LossFunction LOSSFUNCTION = LossFunctions.LossFunction.MEAN_SQUARED_LOGARITHMIC_ERROR;
    public final static String COMMENTS = "just a test";
}
