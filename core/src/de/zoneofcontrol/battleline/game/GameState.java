package de.zoneofcontrol.battleline.game;

import com.badlogic.gdx.utils.Array;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.factory.Nd4j;

import java.util.ArrayList;
import java.util.List;

import static de.zoneofcontrol.battleline.resources.Constants.*;

public class GameState {
    private MainGame.PLAYERS player;
    private Gameboard gameboard;
    private Array<Card> playerhand;
    double winScore;
    int visitCount;
    Deck leftCards;

    public GameState(Gameboard gameboard, Array<Card> phand, MainGame.PLAYERS player) {
        this.player = player;
        this.gameboard = gameboard;
        this.playerhand = phand;
    }

    public Integer[] getPlayableFlags() {
        return gameboard.getUnclaimedFlagPositions(player);
    }

    public List<GameState> getAllPossibleChildStates() {
        List<GameState> allPossibleStates = new ArrayList<>();
        for (int i = 0; i < leftCards.getDeckSize(); i++) {
            for (int j = 0; j < FLAGS; j++) {
                Array<Card> playerFlagCards = player == MainGame.PLAYERS.PLAYER1 ?
                        gameboard.getFlags()[j].getPlayer1cards() : gameboard.getFlags()[j].getPlayer2cards();
                if (gameboard.getFlags()[j].getWinner() != 0 || playerFlagCards.size > 2) {
                    continue;
                }
                Gameboard newGB = new Gameboard(gameboard.getFlags());
                Deck newDeck = new Deck(leftCards);
                Card playedCard = newDeck.getCards().get(i);
                newDeck.getCards().removeIndex(i);
                newGB.getFlags()[j].playCard(playedCard, player);
                GameState childState = new GameState(newGB, playerhand, player);
                childState.setLeftCards(newDeck);
                allPossibleStates.add(childState);
            }
        }
        return allPossibleStates;
    }

    public int getNumberofplayercards() {
        return playerhand.size;
    }

    public Array<Card> getPlayerhand() {
        return playerhand;
    }

    public void incrementVisit() {
        visitCount++;
    }

    public void incrementScore() {
        winScore++;
    }

    public int getVisitCount() {
        return visitCount;
    }

    public void setVisitCount(int visitCount) {
        this.visitCount = visitCount;
    }

    public double getWinScore() {
        return winScore;
    }

    public void setWinScore(double winScore) {
        this.winScore = winScore;
    }

    public void setLeftCards(Deck leftCards) {
        this.leftCards = leftCards;
    }

    public Deck getLeftCards() {
        return leftCards;
    }

    public INDArray getSingleFlagInputs(Deck remainingCards, MainGame.PLAYERS player, Card card, int flag,  Array<Card> playerhand) {
        INDArray twoDArray;
        double[] input={};
        if(DELTA_MAX_SCORE>0){
            input=Utils.concatenateTwoArrays(input,getInputDeltaMaxScore(remainingCards, gameboard.getFlags()[flag], player, card));
        }
        
        if(HOW_MANY_CONS_NUMBERS_IN_DECK>0){
            input=Utils.concatenateTwoArrays(input,getInputConsecutiveNumbersInDeck(card,remainingCards.getCards()));
        }
        if(HOW_MANY_SAME_NUMBERS_IN_DECK>0){
            input=Utils.concatenateTwoArrays(input,getInputSameNumbersInDeck(card,remainingCards.getCards()));
        }
        if(HOW_MANY_SAME_NUMBERS_IN_HAND>0){
            input=Utils.concatenateTwoArrays(input,getInputSameNumbersInHand(card,playerhand));
        }
        if(HOW_MANY_CONS_NUMBERS_IN_HAND>0){
            input=Utils.concatenateTwoArrays(input,getInputConsecutiveNumbersInHand(card,playerhand));
        }
        if(HOW_MANY_SAME_COLORS_IN_DECK>0){
            input=Utils.concatenateTwoArrays(input,getInputSameColorsInDeck(card,remainingCards.getCards()));
        }
        if(HOW_MANY_SAME_COLORS_IN_HAND>0){
            input=Utils.concatenateTwoArrays(input,getInputSameColorsInHand(card,playerhand));
        }
        if(WILL_CARD_WIN_FLAG>0){
            input=Utils.concatenateTwoArrays(input,getInputWillCardWinFlag());
        }
        if(WILL_CARD_WIN_GAME>0){
            input=Utils.concatenateTwoArrays(input,getInputWillCardWinGame());
        }
        twoDArray = Nd4j.create(input, new int[]{1, NUMBER_INPUTS});
        return twoDArray;
    }

    public static double[] getInputDeltaMaxScore(Deck deck, Flag flag, MainGame.PLAYERS player, Card card) {
        double[] input = new double[1];
        input[0] = Utils.getDeltaMaxScore(deck, card, new Flag(flag), player);
        return input;
    }

    private double[] getInputConsecutiveNumbersInDeck(Card c, Array<Card> deck) {
        double[] input = new double[1];
        input[0]=Utils.getAdjacentCards(c,deck);
        return input;
    }

    private double[] getInputSameNumbersInDeck(Card c, Array<Card> deck) {
        double[] input = new double[1];
        float maxSameNumbers=COLORS-1;
        input[0]=Utils.getAmountOfSameNumberCards(c,deck)/maxSameNumbers;
        return input;
    }

    private double[] getInputSameNumbersInHand(Card c, Array<Card> deck) {
        double[] input = new double[1];
        float maxSameNumbers=COLORS-1;
        input[0]=Utils.getAmountOfSameNumberCards(c,deck)/maxSameNumbers;
        return input;
    }

    private double[] getInputConsecutiveNumbersInHand(Card c, Array<Card> hand) {
        double[] input = new double[1];
        input[0]=Utils.getAdjacentCards(c,hand);
        return input;
    }

    private double[] getInputSameColorsInDeck(Card c, Array<Card> deck) {
        double[] input = new double[1];
        float maxSameColors=CARD_NUMBERS-1;
        input[0]=Utils.getAmountOfSameColorCards(c,deck)/maxSameColors;
        return input;
    }

    private double[] getInputSameColorsInHand(Card c, Array<Card> deck) {
        double[] input = new double[1];
        float maxSameColors=CARD_NUMBERS-1;
        if(maxSameColors>HANDSIZE-1){
            maxSameColors=HANDSIZE-1;
        }
        input[0]=Utils.getAmountOfSameColorCards(c,deck)/maxSameColors;
        return input;
    }

    private double[] getInputWillCardWinFlag() {
        return null;
    }

    private double[] getInputWillCardWinGame() {
        return null;
    }

    public Gameboard getGameboard() {
        return gameboard;
    }
}
