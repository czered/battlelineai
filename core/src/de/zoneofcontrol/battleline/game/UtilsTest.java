package de.zoneofcontrol.battleline.game;

import org.junit.Assert;
import org.junit.Test;

import static de.zoneofcontrol.battleline.resources.Constants.FLAGS;
import static org.junit.Assert.*;

public class UtilsTest {

    @Test
    public void checkThatGetBiggestIndexWorksWithNormalArrays() {
        double[] indexes = {1, 2, 3, 4, 1, 10, 0, 2};
        int biggestIndex = Utils.getBiggestIndex(indexes);
        Assert.assertEquals(5, biggestIndex);
    }

    @Test(expected = IllegalArgumentException.class)
    public void checkThatGetBiggestIndexThrowsErrorWithEmptyArray() {
        double[] indexes = {};
        Utils.getBiggestIndex(indexes);
    }

    @Test
    public void checkIfGameOver_noWinner_1() {
        Flag[] flags = new Flag[FLAGS];
        for(int i = 0; i < FLAGS; i++) {
            flags[i] = new Flag();
        }
        int winner = Utils.isGameover(flags);
        Assert.assertEquals(0, winner);
    }

    @Test
    public void checkIfGameOver_noWinner_2() {
        Flag[] flags = new Flag[FLAGS];
        for(int i = 0; i < FLAGS; i++) {
            flags[i] = new Flag();
        }
        flags[0].setWinner(1);
        flags[2].setWinner(1);
        flags[4].setWinner(1);
        flags[6].setWinner(1);
        int winner = Utils.isGameover(flags);
        Assert.assertEquals(0, winner);
    }

    @Test
    public void checkIfGameOver_noWinner_3() {
        Flag[] flags = new Flag[FLAGS];
        for(int i = 0; i < FLAGS; i++) {
            flags[i] = new Flag();
        }
        flags[0].setWinner(1);
        flags[1].setWinner(2);
        flags[2].setWinner(1);
        flags[3].setWinner(2);
        flags[4].setWinner(1);
        flags[5].setWinner(2);
        flags[6].setWinner(1);
        flags[7].setWinner(2);
        int winner = Utils.isGameover(flags);
        Assert.assertEquals(0, winner);
    }

    @Test
    public void checkIfGameOver_noWinner_4() {
        Flag[] flags = new Flag[FLAGS];
        for(int i = 0; i < FLAGS; i++) {
            flags[i] = new Flag();
        }
        flags[0].setWinner(1);
        flags[1].setWinner(1);
        flags[2].setWinner(2);
        flags[3].setWinner(2);
        flags[4].setWinner(1);
        flags[5].setWinner(1);
        flags[6].setWinner(2);
        flags[7].setWinner(2);
        int winner = Utils.isGameover(flags);
        Assert.assertEquals(0, winner);
    }

    @Test
    public void checkIfGameOver_FirstPlayerWins_1() {
        Flag[] flags = new Flag[FLAGS];
        for(int i = 0; i < FLAGS; i++) {
            flags[i] = new Flag();
        }
        flags[0].setWinner(1);
        flags[1].setWinner(1);
        flags[2].setWinner(2);
        flags[3].setWinner(2);
        flags[4].setWinner(1);
        flags[5].setWinner(1);
        flags[6].setWinner(2);
        flags[7].setWinner(2);
        flags[8].setWinner(1);
        int winner = Utils.isGameover(flags);
        Assert.assertEquals(1, winner);
    }

    @Test
    public void checkIfGameOver_FirstPlayerWins_3() {
        Flag[] flags = new Flag[FLAGS];
        for(int i = 0; i < FLAGS; i++) {
            flags[i] = new Flag();
        }
        flags[0].setWinner(1);
        flags[1].setWinner(1);
        flags[2].setWinner(1);
        int winner = Utils.isGameover(flags);
        Assert.assertEquals(1, winner);
    }

    @Test
    public void checkIfGameOver_FirstPlayerWins_4() {
        Flag[] flags = new Flag[FLAGS];
        for(int i = 0; i < FLAGS; i++) {
            flags[i] = new Flag();
        }
        flags[5].setWinner(1);
        flags[6].setWinner(1);
        flags[7].setWinner(1);
        int winner = Utils.isGameover(flags);
        Assert.assertEquals(1, winner);
    }

    @Test
    public void checkIfGameOver_FirstPlayerWins_2() {
        Flag[] flags = new Flag[FLAGS];
        for(int i = 0; i < FLAGS; i++) {
            flags[i] = new Flag();
        }
        flags[0].setWinner(1);
        flags[1].setWinner(1);
        flags[2].setWinner(2);
        flags[3].setWinner(2);
        flags[4].setWinner(1);
        flags[5].setWinner(1);
        flags[6].setWinner(1);
        int winner = Utils.isGameover(flags);
        Assert.assertEquals(1, winner);
    }

    @Test
    public void checkIfGameOver_SecondPlayerWins_1() {
        Flag[] flags = new Flag[FLAGS];
        for(int i = 0; i < FLAGS; i++) {
            flags[i] = new Flag();
        }
        flags[0].setWinner(1);
        flags[1].setWinner(1);
        flags[2].setWinner(2);
        flags[3].setWinner(2);
        flags[4].setWinner(1);
        flags[5].setWinner(1);
        flags[6].setWinner(2);
        flags[7].setWinner(2);
        flags[8].setWinner(2);
        int winner = Utils.isGameover(flags);
        Assert.assertEquals(2, winner);
    }

    @Test
    public void checkIfGameOver_SecondPlayerWins_2() {
        Flag[] flags = new Flag[FLAGS];
        for(int i = 0; i < FLAGS; i++) {
            flags[i] = new Flag();
        }
        flags[0].setWinner(2);
        flags[1].setWinner(1);
        flags[2].setWinner(2);
        flags[3].setWinner(2);
        flags[4].setWinner(1);
        flags[5].setWinner(2);
        flags[6].setWinner(2);
        int winner = Utils.isGameover(flags);
        Assert.assertEquals(2, winner);
    }

    @Test
    public void checkIfGameOver_SecondPlayerWins_3() {
        Flag[] flags = new Flag[FLAGS];
        for(int i = 0; i < FLAGS; i++) {
            flags[i] = new Flag();
        }
        flags[5].setWinner(2);
        flags[6].setWinner(2);
        flags[7].setWinner(2);
        int winner = Utils.isGameover(flags);
        Assert.assertEquals(2, winner);
    }
}