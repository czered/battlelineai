package de.zoneofcontrol.battleline.game;

import com.badlogic.gdx.utils.Array;

import java.util.ArrayList;

import static de.zoneofcontrol.battleline.resources.Constants.*;

public class Gameboard {

    private Flag[] flags;

    Gameboard(int flags) {
        this.flags = new Flag[flags];
        for (int i = 0; i < this.flags.length; i++) {
            this.flags[i] = new Flag();
        }
    }

    Gameboard(Flag[] _flags) {
        Flag[] newFlags = new Flag[_flags.length];
        for (int i = 0; i < _flags.length; i++) {
            Array<Card> cards1 = new Array<>(_flags[i].getPlayer1cards());
            Array<Card> cards2 = new Array<>(_flags[i].getPlayer2cards());
            newFlags[i] = new Flag(cards1, cards2, _flags[i].getWinner());
        }
        flags = newFlags;
    }

    int isGameover() {
        return Utils.isGameover(flags);
    }

    public Flag[] getFlags() {
        return flags;
    }

    public Flag getFlag(int i) {
        return flags[i];
    }

    public Integer[] getUnclaimedFlagPositions(MainGame.PLAYERS currentPlayer) {
        ArrayList<Integer> positions = new ArrayList<>();
        for (int i = 0; i < flags.length; i++) {
            int playerFlagCards = currentPlayer == MainGame.PLAYERS.PLAYER1 ? flags[i].getPlayer1cards().size : flags[i].getPlayer2cards().size;
            if (flags[i].getWinner() == 0 && playerFlagCards < 3) {
                positions.add(i);
            }
        }
        return positions.toArray(new Integer[positions.size()]);
    }

    public double[] getCurrentState(MainGame.PLAYERS player) {
        if (player == MainGame.PLAYERS.PLAYER1) {
            return Utils.concatenateTwoArrays(getPlayer1State(), getPlayer2State());
        } else {
            return Utils.concatenateTwoArrays(getPlayer2State(), getPlayer1State());
        }
    }

    private double[] getPlayer1State() {
        double[] playerState = new double[0];
        for (Flag flag : flags) {
            playerState = Utils.concatenateTwoArrays(playerState, getFlagState(flag.getPlayer1cards()));
        }
        return playerState;
    }

    private double[] getPlayer2State() {
        double[] playerState = new double[0];
        for (Flag flag : flags) {
            playerState = Utils.concatenateTwoArrays(playerState, getFlagState(flag.getPlayer2cards()));
        }
        return playerState;
    }

    private double[] getFlagState(Array<Card> cards) {
        if (cards.size == 0) {
            return Utils.getArrayOfZEROS(BITS_PER_CARDS * 3);
        } else if (cards.size == 1) {
            return Utils.concatenateThreeArrays(cards.get(0).getBinaryRepresentation(), Utils.getArrayOfZEROS(BITS_PER_CARDS), Utils.getArrayOfZEROS(BITS_PER_CARDS));
        } else if (cards.size == 2) {
            return Utils.concatenateThreeArrays(cards.get(0).getBinaryRepresentation(),
                    cards.get(1).getBinaryRepresentation(), Utils.getArrayOfZEROS(BITS_PER_CARDS));
        } else {
            return Utils.concatenateThreeArrays(cards.get(0).getBinaryRepresentation(),
                    cards.get(1).getBinaryRepresentation(),
                    cards.get(2).getBinaryRepresentation());
        }
    }

    public int[] getPlayersScore() {
        return new int[]{getPlayer1Score(), getPlayer2Score()};
    }

    private int getPlayer1Score() {
        int score = 0;
        for (Flag flag : flags) {
            score += flag.getP1score();
        }
        return score;
    }

    private int getPlayer2Score() {
        int score = 0;
        for (Flag flag : flags) {
            score += flag.getP2score();
        }
        return score;
    }

    public void printFlags() {
        for (Flag f : flags) {
            System.out.println(f.getVisualization());
        }
    }
}
