package de.zoneofcontrol.battleline.game;

import java.util.Random;

import com.badlogic.gdx.utils.Array;

import de.zoneofcontrol.battleline.ai.Agent;
import de.zoneofcontrol.battleline.ai.Move;
import de.zoneofcontrol.battleline.ai.montecarlo.Node;

import static de.zoneofcontrol.battleline.resources.Constants.*;

public class MainGame {

    private boolean playerPassed = false;

    public enum BATTLELINE_STATES {
        PICK_CARD, PICK_POSITION, DRAW_CARD, GAME_OVER
    }

    public enum PLAYERS {
        PLAYER1(1), PLAYER2(2);

        public final int value;

        PLAYERS(int value) {
            this.value = value;
        }
    }

    private GameTurn lastGameTurn;

    private Deck deck; // the drawing deck

    private Array<Card> phand1; // player 1s hand cards
    private Array<Card> phand2; // player 2s hand cards
    private Gameboard gb; // Contains all our flags
    private PLAYERS agentPlayer;
    private PLAYERS currentplayer = PLAYERS.PLAYER1;
    private BATTLELINE_STATES playerturnstate = BATTLELINE_STATES.PICK_CARD;
    private Card selectedCard = null;
    private Array<Card> currentHand = null;
    private Random rdm;
    private Move selectedMove = null;
    private boolean playerWonFlag = false;
    private boolean playerLostFlag = false;
    private int flagClaimed = -1;

    private int winner = 0;
    private int selectedCardPosition;
    private int selectedCardId;
    private int selectedFlagPosition;
    private double numberOfCardsPlayedInFlag;

    private boolean shuffleHand = true;

    // This gets called once on program start
    public void create() {
        rdm = new Random();
        deck = new Deck();
        phand1 = new Array<>();
        phand2 = new Array<>();
    }

    // Resets all variables for a new game
    public void createNewGame(PLAYERS _agentPlayer, int handsize) {
        agentPlayer = _agentPlayer;
        currentplayer = PLAYERS.PLAYER1;
        playerturnstate = BATTLELINE_STATES.PICK_CARD;
        phand1.clear();
        phand2.clear();

        currentHand = phand1;
        gb = new Gameboard(FLAGS);

        deck.resetDeck();
        // Deal initial cards
        for (int i = 0; i < handsize; i++) {
            phand1.add(deck.getSingleCard());
            phand2.add(deck.getSingleCard());
        }
    }

    private void finishGameTurn() {
        if (playerturnstate == BATTLELINE_STATES.DRAW_CARD) {
            claimFlags();
            if (!deck.isEmpty() && currentHand.size < HANDSIZE) {
                currentHand.add(deck.getSingleCard());
            }
            if (shuffleHand) {
                currentHand.shuffle();
            }
            switchPlayers();
            playerturnstate = BATTLELINE_STATES.PICK_CARD;
            checkEndGame();
        }
    }

    public void executeAITurn(Agent agent) {
        Move move = selectBestMove(findMovesForEachFlag(agent));
        if (move != null) {
            selectedMove = move;
            numberInput(getNumberOfCard(move.getCard()));
            numberInput(move.getFlag());
        } else {

            switchPlayers();
        }
        finishGameTurn();
 //       printGameboard();
    }


    private int getNumberOfCard(Card card) {
        return getCurrentHand().indexOf(card,false);
    }

    private Array<Move> findMovesForEachFlag(Agent agent) {
        Array<Move> moves = new Array<Move>();
        int agentCardSize = agent.getPlayer() == PLAYERS.PLAYER1 ? phand1.size : phand2.size;
        float random = rdm.nextFloat();

        Deck leftCards = new Deck(deck);
        leftCards.addCards(phand2);
        leftCards.addCards(phand1);
        for (int i = 0; i < FLAGS; i++) {
            if(gb.getFlags()[i].isPlayable(agent.getPlayer())) {
                Move move = agent.getMove(getState(), agentCardSize, random, leftCards, getCurrentHand(), i);
                if (move != null) {
                    moves.add(move);
                }
            }
        }
        return moves;
    }

    private Array<Card> getCurrentHand() {
        if(currentplayer==PLAYERS.PLAYER1)
            return phand1;
        else
            return phand2;
    }

    private Move selectBestMove(Array<Move> moves) {
//        printMovesQ(moves);
        Move move = null;
        if (moves.size > 0) {
            move = moves.random();
            if(move.isRandom()){
                return moves.random();
            }
            for (Move m : moves) {
                if (m.getQ() > move.getQ()) {
                    move = m;
                }
            }
        }
        return move;
    }

    private void printMovesQ(Array<Move> moves) {
        for (Move m:moves) {
            System.out.println(m.toString());
        }
    }


    private void switchPlayers() {
        playerturnstate = BATTLELINE_STATES.PICK_CARD;
        switch (currentplayer) {
            case PLAYER1:
                currentHand = phand2;
                currentplayer = PLAYERS.PLAYER2;
                break;
            case PLAYER2:
                currentHand = phand1;
                currentplayer = PLAYERS.PLAYER1;

        }
    }

    private void checkEndGame() {
        winner = gb.isGameover();
        if (winner != 0) {
//            System.out.printf("GAME OVER. The winner is player %s\n ", winner);
            playerturnstate = BATTLELINE_STATES.GAME_OVER;
        }
    }


    // This function takes a given input and checks if it is a legal move, if it is
    // it performs the move
    private void numberInput(int userInput) {
        switch (playerturnstate) {
            case PICK_CARD:
                selectCard(userInput);
                break;
            case PICK_POSITION:
                selectPosition(userInput);
                break;
        }
    }

    private void selectCard(int userInput) {
        if (currentHand.size <= userInput || userInput < 0) {
            System.out.println("!!!!!!!!!!!!ERROR CARD NOT AVAILABLE!!!!!!!!!!!!");
            System.out.println("Selected Card: " + userInput + " handsize: " + currentHand.size);
            return;
        }
        selectedCard = currentHand.get(userInput);
        if (selectedCard != null) {
            selectedCardPosition = userInput;
            playerturnstate = BATTLELINE_STATES.PICK_POSITION;
        }
    }

    private void selectPosition(int userInput) {
        playerLostFlag = false;
        playerWonFlag = false;
        if (gb.getFlags()[userInput].isPlayable(currentplayer)) {
            selectedFlagPosition = userInput;
            selectedCardId = currentHand.get(selectedCardPosition).getId();
            checkNumberOfCardsInFlag();
            saveTurn();
            int winner = gb.getFlags()[userInput].playCard(selectedCard, currentplayer);
            playerturnstate = BATTLELINE_STATES.DRAW_CARD;
            currentHand.removeValue(selectedCard, false);
            selectedCard = null;
            if (winner == currentplayer.value) {
                playerWonFlag = true;
            } else if (winner != 0) {
                playerLostFlag = true;
            }
        }
    }

    private void claimFlags() {
        flagClaimed = -1;
        for (int i = 0; i < gb.getFlags().length; i++) {
            if (needToBeChecked(gb.getFlags()[i]) && canBeClaimed(gb.getFlags()[i])) {
                gb.getFlags()[i].setWinner(currentplayer == PLAYERS.PLAYER1 ? 1 : 2);
                flagClaimed = i;
            }
        }
    }

    private boolean canBeClaimed(Flag flag) {
        int currentPlayerScore;
        Deck leftCards = new Deck(deck);

        switch (currentplayer) {
            case PLAYER1:
            default:
                currentPlayerScore = flag.getP1score();
                leftCards.addCards(phand2);
                break;
            case PLAYER2:
                currentPlayerScore = flag.getP2score();
                leftCards.addCards(phand1);
                break;
        }

        int adversaryMaxPossibleScore = Utils.getMaxOpposingPossibleScore(leftCards, flag, currentplayer);
        return adversaryMaxPossibleScore <= currentPlayerScore;
    }

    private boolean needToBeChecked(Flag f) {
        if (f.getWinner() != 0) {
            return false;
        }
        switch (currentplayer) {
            case PLAYER1:
                if (f.getPlayer1cards().size < 3) {
                    return false;
                }
                break;
            case PLAYER2:
                if (f.getPlayer2cards().size < 3) {
                    return false;
                }
                break;
        }
        return true;
    }

    private void checkNumberOfCardsInFlag() {
        switch (currentplayer) {
            case PLAYER1:
                numberOfCardsPlayedInFlag = gb.getFlags()[selectedFlagPosition].getPlayer1cards().size + 1;
                break;
            case PLAYER2:
                numberOfCardsPlayedInFlag = gb.getFlags()[selectedFlagPosition].getPlayer2cards().size + 1;
                break;
        }
    }

    private void saveTurn() {
        lastGameTurn = new GameTurn(getState(), getAction(), selectedMove);
    }

    private GameState getState() {
        Gameboard newGb = new Gameboard(gb.getFlags());
        GameState gs = new GameState(newGb, currentHand, currentplayer);
        return gs;
    }

    private GameState getSingleFlagState(int currentflag) {
        Flag[] singleflag = new Flag[1];
        singleflag[0] = gb.getFlag(currentflag);
        Gameboard newGb = new Gameboard(singleflag);
        GameState gs = new GameState(newGb, currentHand, currentplayer);
        return gs;
    }

    private Action getAction() {
        Action a = new Action(getActionSelected(), selectedFlagPosition, numberOfCardsPlayedInFlag);
        return a;
    }

    public void createNewGameFromNode(Node nodeToExplore) {
        currentplayer = nodeToExplore.getPlayer() == MainGame.PLAYERS.PLAYER1 ? MainGame.PLAYERS.PLAYER2 : MainGame.PLAYERS.PLAYER1;
        playerturnstate = BATTLELINE_STATES.PICK_CARD;
        phand1.clear();
        phand2.clear();

        currentHand = phand1;
        deck = new Deck(nodeToExplore.getState().getLeftCards());
        // Deal initial cards
        if (currentplayer == PLAYERS.PLAYER1) {
            for (int i = 0; i < HANDSIZE; i++) {
                if (!deck.isEmpty()) {
                    phand1.add(deck.getSingleCard());
                }
                if (!deck.isEmpty()) {
                    phand2.add(deck.getSingleCard());
                }
            }
        } else {
            for (int i = 0; i < HANDSIZE; i++) {
                if (!deck.isEmpty()) {
                    phand2.add(deck.getSingleCard());
                }
                if (!deck.isEmpty()) {
                    phand1.add(deck.getSingleCard());
                }
            }
        }
    }

    public int getWinner() {
        return winner;
    }

    public PLAYERS getCurrentplayer() {
        return currentplayer;
    }

    public BATTLELINE_STATES getPlayerturnstate() {
        return playerturnstate;
    }

    public Gameboard getGameBoard() {
        return gb;
    }

    public boolean didPlayerWinFlag() {
        return playerWonFlag;
    }


    public boolean didPlayerLostFlag() {
        return playerLostFlag;
    }

    public int getActionSelected() {
        return (selectedFlagPosition * DECK_SIZE) + selectedCardId;
    }

    public int isFlagClaimed() {
        return flagClaimed;
    }

    public int getSelectedFlagPosition() {
        return selectedFlagPosition;
    }

    public GameTurn getLastGameTurn() {
        return lastGameTurn;
    }

    public Deck getDeck() {
        return deck;
    }

    public boolean didPlayerPass() {
        return playerPassed;
    }

    public void printGameboard() {
        System.out.println("=============================");
        String info=" Current player "+ currentplayer;
        String handp1 = "Hand P1: ";
        String handp2 = "Hand P2: ";
        for (Card c : phand1) {
            handp1 += c.toString() + "  ";
        }
        for (Card c : phand2) {
            handp2 += c.toString() + "  ";

        }
        System.out.println(info);
        System.out.println(handp1);
        System.out.println(handp2);
        System.out.println("-----------------------------");
        getGameBoard().printFlags();
        System.out.println("-----------------------------");
        System.out.println("Winner: " + winner);
        System.out.println("Score P1-P2:  " + gb.getPlayersScore()[0] + "  -  " + gb.getPlayersScore()[1]);
        System.out.println("=============================");
    }
}
