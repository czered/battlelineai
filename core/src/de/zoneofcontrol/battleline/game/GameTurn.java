package de.zoneofcontrol.battleline.game;

import de.zoneofcontrol.battleline.ai.Move;

public class GameTurn {

    private GameState state;
    private Action action;
    private Move move;

    GameTurn(GameState s, Action a, Move m) {
        this.state = s;
        this.action = a;
        this.move = m;
    }

    public int getActionTaken() {
        return action.getActionTaken();
    }

    public double getReward() {
        return action.getReward();
    }

    public void setReward(double reward) {
        action.setReward(reward);
    }

    public int getFlagPlayed() {
        return action.getFlagPlayed();
    }

    public double getNumberOfCardsInFlag() {
        return action.getNumberOfCardsInFlag();
    }

    public GameState getState() {
        return state;
    }

    public Action getAction() {
        return action;
    }

    public Move getMove() {
        return move;
    }

}
