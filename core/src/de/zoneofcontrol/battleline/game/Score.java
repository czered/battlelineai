package de.zoneofcontrol.battleline.game;

public class Score {

    private int player1score;
    private int player2score;
    private double totalReward;
    private int numberOfTurns;

    public Score(int player1score, int player2score, double totalReward, int numberOfTurns) {
        this.player1score = player1score;
        this.player2score = player2score;
        this.totalReward = totalReward;
        this.numberOfTurns = numberOfTurns;
    }

    public int getPlayer1score() {
        return player1score;
    }

    public void setPlayer1score(int player1score) {
        this.player1score = player1score;
    }

    public int getPlayer2score() {
        return player2score;
    }

    public void setPlayer2score(int player2score) {
        this.player2score = player2score;
    }

    public double getTotalReward() {
        return totalReward;
    }

    public void setTotalReward(double totalReward) {
        this.totalReward = totalReward;
    }



}
