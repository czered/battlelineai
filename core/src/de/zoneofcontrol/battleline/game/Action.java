package de.zoneofcontrol.battleline.game;

public class Action {
    private int actionTaken;
    private double reward;
    private int flagPlayed;
    private double numberOfCardsInFlag;

    public Action(int actionTaken, int flagPlayed, double numberOfCardsInFlag) {
        this.actionTaken = actionTaken;
        this.flagPlayed = flagPlayed;
        this.numberOfCardsInFlag = numberOfCardsInFlag;
        this.reward = 0;
    }

    public int getActionTaken() {
        return actionTaken;
    }

    public void setActionTaken(int actionTaken) {
        this.actionTaken = actionTaken;
    }

    public double getReward() {
        return reward;
    }

    public void setReward(double reward) {
        this.reward = reward;
    }

    public int getFlagPlayed() {
        return flagPlayed;
    }

    public void setFlagPlayed(int flagPlayed) {
        this.flagPlayed = flagPlayed;
    }

    public double getNumberOfCardsInFlag() {
        return numberOfCardsInFlag;
    }

    public void setNumberOfCardsInFlag(double numberOfCardsInFlag) {
        this.numberOfCardsInFlag = numberOfCardsInFlag;
    }

}
