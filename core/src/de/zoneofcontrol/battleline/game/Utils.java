package de.zoneofcontrol.battleline.game;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import com.badlogic.gdx.utils.Array;
import de.zoneofcontrol.battleline.ai.montecarlo.Node;
import org.jetbrains.annotations.NotNull;

import static de.zoneofcontrol.battleline.resources.Constants.*;


public class Utils {

    static double[] getBinaryColor(int color) {
        switch (color) {
            case (0):
                return new double[]{ONE, ZERO, ZERO, ZERO, ZERO, ZERO};
            case (1):
                return new double[]{ZERO, ONE, ZERO, ZERO, ZERO, ZERO};
            case (2):
                return new double[]{ZERO, ZERO, ONE, ZERO, ZERO, ZERO};
            case (3):
                return new double[]{ZERO, ZERO, ZERO, ONE, ZERO, ZERO};
            case (4):
                return new double[]{ZERO, ZERO, ZERO, ZERO, ONE, ZERO};
            case (5):
            default:
                return new double[]{ZERO, ZERO, ZERO, ZERO, ZERO, ONE};
        }
    }

    static double[] getBinaryNumber(int number) {
        switch (number) {
            case (1):
                return new double[]{ONE, ZERO, ZERO, ZERO, ZERO, ZERO, ZERO, ZERO, ZERO, ZERO};
            case (2):
                return new double[]{ZERO, ONE, ZERO, ZERO, ZERO, ZERO, ZERO, ZERO, ZERO, ZERO};
            case (3):
                return new double[]{ZERO, ZERO, ONE, ZERO, ZERO, ZERO, ZERO, ZERO, ZERO, ZERO};
            case (4):
                return new double[]{ZERO, ZERO, ZERO, ONE, ZERO, ZERO, ZERO, ZERO, ZERO, ZERO};
            case (5):
                return new double[]{ZERO, ZERO, ZERO, ZERO, ONE, ZERO, ZERO, ZERO, ZERO, ZERO};
            case (6):
                return new double[]{ZERO, ZERO, ZERO, ZERO, ZERO, ONE, ZERO, ZERO, ZERO, ZERO};
            case (7):
                return new double[]{ZERO, ZERO, ZERO, ZERO, ZERO, ZERO, ONE, ZERO, ZERO, ZERO};
            case (8):
                return new double[]{ZERO, ZERO, ZERO, ZERO, ZERO, ZERO, ZERO, ONE, ZERO, ZERO};
            case (9):
                return new double[]{ZERO, ZERO, ZERO, ZERO, ZERO, ZERO, ZERO, ZERO, ONE, ZERO};
            case (10):
            default:
                return new double[]{ZERO, ZERO, ZERO, ZERO, ZERO, ZERO, ZERO, ZERO, ZERO, ONE};
        }
    }

    public static double[] getArrayOfZEROS(int size) {
        double[] array = new double[size];
        for (int i = 0; i < size; i++) {
            array[i] = ZERO;
        }
        return array;
    }

    public static double[] getStateFromCards(Array<Card> cards) {
        double[] handState = new double[0];
        for (int i = 0; i < cards.size; i++) {
            handState = concatenateTwoArrays(handState, cards.get(i).getBinaryRepresentation());
        }
        int finalSize = cards.size;
        while (finalSize < 7) {
            handState = concatenateTwoArrays(handState, new double[BITS_PER_CARDS]);
            finalSize++;
        }
        return handState;
    }

    public static double[] getState60FromCards(Array<Card> cards) {
        double[] handState = new double[DECK_SIZE];
        for (int i = 0; i < DECK_SIZE; i++) {
            handState[i] = -0.3603807;
        }
        for (Card card : cards) {
            handState[card.getId()] = 2.7285964;
        }
        return handState;
    }

    public static double getDeltaMaxScore(Deck deck, Card card, Flag flag, MainGame.PLAYERS player) {
        double difference = 0;

        if (flag.isPlayable(player)) {
            int initialmaxscore = getMaxOwnPossibleScore(deck, flag, player);
            flag.fakeplayCard(card, player);
            int newscore = 0;
            if (flag.isPlayable(player)) { //check the maximum possible Score if there is still room to play
                newscore = getMaxOwnPossibleScore(deck, flag, player);
            } else { //checkTheFinalScore instead of possible Score if there is no more room for cards
                if (player == MainGame.PLAYERS.PLAYER1) {
                    flag.checkScore(flag.getPlayer1cards(), player);
                    newscore = flag.getP1score();
                } else {
                    flag.checkScore(flag.getPlayer2cards(), player);
                    newscore = flag.getP2score();
                }
            }
            difference = initialmaxscore - newscore;
            difference = difference / 4021;
            difference = 1 - difference;
            return difference;
        } else {
            return 9999;
        }
    }


    public static float getAdjacentCards(Card cardtocheck, Array<Card> cardstocompare) {
        float onehigherexists=0;
        float onelowerexists=0;
        for (Card handCard : cardstocompare) {
            if (handCard.getNumber() == cardtocheck.getNumber() + 1){
                onehigherexists=0.5f;
            }
            if (handCard.getNumber() == cardtocheck.getNumber() - 1){
                onelowerexists=0.5f;
            }
            }

        return onehigherexists+onelowerexists;
    }

    public static float getAmountOfSameNumberCards(Card cardtocheck, Array<Card> cardstocompare) {
        float amount=0;
        for (Card handCard : cardstocompare) {
            if (handCard.getNumber() == cardtocheck.getNumber()){
                amount++;
            }
        }
        return amount;
    }

    public static float getAmountOfSameColorCards(Card cardtocheck, Array<Card> cardstocompare) {
        float amount=0;
        for (Card handCard : cardstocompare) {
            if (handCard.getColor() == cardtocheck.getColor()){
                amount++;
            }
        }
        return amount;
    }


    public static double[] concatenateTwoArrays(double[] array1, double[] array2) {
        double[] result = new double[array1.length + array2.length];
        System.arraycopy(array1, 0, result, 0, array1.length);
        System.arraycopy(array2, 0, result, array1.length, array2.length);
        return result;
    }

    public static double[] concatenateThreeArrays(double[] array1, double[] array2, double[] array3) {
        double[] result = new double[array1.length + array2.length + array3.length];
        System.arraycopy(array1, 0, result, 0, array1.length);
        System.arraycopy(array2, 0, result, array1.length, array2.length);
        System.arraycopy(array3, 0, result, array1.length + array2.length, array3.length);
        return result;
    }

    static int isGameover(Flag[] flags) {
        int winner = checkThreeConsecutiveFlags(flags);
        if (winner != 0) {
            return winner;
        } else {
            return checkFiveFlags(flags);
        }
    }

    private static int checkFiveFlags(Flag[] flags) {
        int flagsP1 = 0;
        int flagsP2 = 0;
        for (Flag flag : flags) {
            if (flag.getWinner() == 1) {
                flagsP1++;
            } else if (flag.getWinner() == 2) {
                flagsP2++;
            }
        }
        if (flagsP1 >= NUM_FLAGS_TO_WIN) {
            return 1;
        } else if (flagsP2 >= NUM_FLAGS_TO_WIN) {
            return 2;
        } else {
            return 0;
        }
    }

    private static int checkThreeConsecutiveFlags(Flag[] flags) {
        int lastFlag = 0;
        int consecutive = 0;
        int winner = 0;
        for (Flag flag : flags) {
            if (flag.getWinner() == 0) {
                lastFlag = 0;
                consecutive = 0;
            } else if (lastFlag == 0) {
                lastFlag = flag.getWinner();
                consecutive++;
            } else if (lastFlag == flag.getWinner()) {
                consecutive++;
            } else {
                consecutive = 0;
                lastFlag = flag.getWinner();
            }
            if (consecutive >= CONSECUTIVES_FLAGS_NEED) {
                winner = flag.getWinner();
                break;
            }
        }
        return winner;
    }

    static int getMaxOwnPossibleScore(Deck deck, Flag flag, MainGame.PLAYERS currentPlayer) {
        Array<Card> cardsinflag;
        switch (currentPlayer) {
            case PLAYER2:
            default:
                cardsinflag = flag.getPlayer2cards();
                break;
            case PLAYER1:
                cardsinflag = flag.getPlayer1cards();
                break;
        }
        return getMaxScoreDumb(deck, cardsinflag);
    }

    static int getMaxOpposingPossibleScore(Deck deck, Flag flag, @NotNull MainGame.PLAYERS currentPlayer) {
        Array<Card> adversaryCards;
        int playerScore;
        switch (currentPlayer) {
            case PLAYER1:
            default:
                adversaryCards = flag.getPlayer2cards();
                playerScore = flag.getP1score();
                break;
            case PLAYER2:
                adversaryCards = flag.getPlayer1cards();
                playerScore = flag.getP2score();
                break;
        }
        return getMaxScoreSmart(deck, adversaryCards, playerScore);
    }

    //WEDGE:      Three consecutive cards with the same color
    //PHALANX:    Three cards with the same number
    //BATTALION:  Three cards of the same color
    //SKIRMISH:   Three consecutive cards different colors
    //HOST:       Any other combination of cards
    private static int getMaxScoreDumb(Deck availableCards, Array<Card> cardsInFlag) {
        int maxScore = -1;
        if (isWedgePossible(cardsInFlag)) {
            maxScore = getMaxPossibleWedge(availableCards, cardsInFlag);
            if (maxScore != -1) {
                return maxScore;
            }
        }

        if (isPhalanxPossible(cardsInFlag)) {
            maxScore = getMaxPossiblePhalanx(availableCards, cardsInFlag);
            if (maxScore != -1) {
                return maxScore;
            }
        }


        if (isBattalionPossible(cardsInFlag)) {
            maxScore = getMaxPossibleBattalion(availableCards, cardsInFlag);
            if (maxScore != -1) {
                return maxScore;
            }
        }

        if (isSkirmishPossible(cardsInFlag)) {
            maxScore = getMaxPossibleSkirmish(availableCards, cardsInFlag);
            if (maxScore != -1) {
                return maxScore;
            }
        }

        return getMaxPossibleHost(availableCards, cardsInFlag);

    }


    private static int getMaxScoreSmart(Deck availableCards, Array<Card> cardsInFlag, int playerScore) {
        int maxScore = isWedgePossible(cardsInFlag) && availableCards.isBigEnough() ? getMaxPossibleWedge(availableCards, cardsInFlag)
                : -1;
        if (maxScore != -1) {
            return maxScore;
        }
        if (playerScore < 4000 && availableCards.isBigEnough()) {
            if (isPhalanxPossible(cardsInFlag)) {
                maxScore = getMaxPossiblePhalanx(availableCards, cardsInFlag);
                if (maxScore != -1) {
                    return maxScore;
                }
            }
        } else {
            return -1;
        }
        if (playerScore < 3000 && availableCards.isBigEnough()) {
            if (isBattalionPossible(cardsInFlag)) {
                maxScore = getMaxPossibleBattalion(availableCards, cardsInFlag);
                if (maxScore != -1) {
                    return maxScore;
                }
            }
        } else {
            return -1;
        }
        if (playerScore < 2000 && availableCards.isBigEnough()) {
            if (isSkirmishPossible(cardsInFlag)) {
                maxScore = getMaxPossibleSkirmish(availableCards, cardsInFlag);
                if (maxScore != -1) {
                    return maxScore;
                }
            }
        } else {
            return -1;
        }
        if (playerScore < 1000 && availableCards.isBigEnough()) {
            return getMaxPossibleHost(availableCards, cardsInFlag);
        } else {
            return -1;
        }
    }

    private static int getMaxPossibleWedge(Deck deck, Array<Card> cardsInFlag) {
        int maxWedge;
        if (cardsInFlag.size > 0) {
            Array<Card> coloredCards = deck.getSingleColorCards(cardsInFlag.get(0).getColor());
            Deck coloredDeck = new Deck(coloredCards);
            maxWedge = getMaxPossibleSkirmish(coloredDeck, cardsInFlag);
            if (maxWedge == -1) {
                return maxWedge;
            }
        } else {
            return getMaxPossibleWedgeEmptyFlag(deck);
        }
        return maxWedge + 3000; //just adding +3000 because the maxPossibleSkirmish already comes with a 1000 tier bonus
    }

    private static int getMaxPossibleWedgeEmptyFlag(Deck deck) {
        int maxWedge = -1;
        Deck newDeck = new Deck(deck.getCards());
        newDeck.sort();
        for (int i = 0; i < newDeck.getDeckSize(); i++) {
            Array<Card> flag = new Array<>();
            flag.add(newDeck.getFirstCard());
            int newWedge = getMaxPossibleWedge(deck, flag);
            if (newWedge > maxWedge) {
                maxWedge = newWedge;
            }
            if (maxWedge == 4027) {
                return maxWedge;
            }
        }
        return maxWedge;
    }

    private static int getMaxPossiblePhalanx(Deck deck, Array<Card> cardsInFlag) {
        if (cardsInFlag.size > 0) {
            Array<Card> numberedCards = deck.getSingleNumberCards(cardsInFlag.get(0).getNumber());
            if (cardsInFlag.size == 1 && numberedCards.size > 1
                    || cardsInFlag.size == 2 && numberedCards.size > 0) {
                return (cardsInFlag.get(0).getNumber() * 3) + 3000;
            } else {
                return -1;
            }
        } else {
            return getMaxPossiblePhalanxFromDeck(deck);
        }
    }

    private static int getMaxPossiblePhalanxFromDeck(Deck deck) {
        for (int i = CARD_NUMBERS; i > 0; i--) {
            if (deck.existsCardWithSpecificNumber(i)) {
                Array<Card> numberedCards = deck.getSingleNumberCards(i);
                if (numberedCards.size > 2) {
                    return (i * 3) + 3000;
                }
            }
        }
        return -1;
    }

    private static int getMaxPossibleBattalion(Deck deck, Array<Card> cardsInFlag) {
        if (cardsInFlag.size > 0) {
            Array<Card> coloredCards = deck.getSingleColorCards(cardsInFlag.get(0).getColor());
            coloredCards.sort();
            if (cardsInFlag.size == 1) {
                if (coloredCards.size > 1) {
                    return cardsInFlag.get(0).getNumber() + coloredCards.get(0).getNumber()
                            + coloredCards.get(1).getNumber() + 2000;
                } else {
                    return -1;
                }
            } else {
                if (coloredCards.size > 0) {
                    return cardsInFlag.get(0).getNumber() + cardsInFlag.get(1).getNumber()
                            + coloredCards.get(0).getNumber() + 2000;
                } else {
                    return -1;
                }
            }
        } else {
            return getMaxPossibleBattalionFromDeck(deck);
        }
    }

    private static int getMaxPossibleBattalionFromDeck(Deck deck) {
        int maxScore = -1;
        for (int i = 0; i < COLORS; i++) {
            Array<Card> coloredCards = deck.getSingleColorCards(i);
            if (coloredCards.size > 2) {
                coloredCards.sort();
                int score = coloredCards.get(0).getNumber() + coloredCards.get(1).getNumber() + coloredCards.get(2).getNumber();
                maxScore = maxScore > score ? maxScore : score;
            }
        }
        if (maxScore == -1) {
            return -1;
        } else {
            return maxScore + 2000;
        }
    }

    private static int getMaxPossibleSkirmish(Deck deck, Array<Card> cardsInFlag) {
        int maxSkirmish;

        if (cardsInFlag.size == 2) {
            if (deck.getCards().size > 0) {
                if (cardsInFlag.get(0).getNumber() == cardsInFlag.get(1).getNumber() + 1) {
                    maxSkirmish = searchExtremeCard(deck, cardsInFlag);
                } else {
                    maxSkirmish = searchMiddleCard(deck, cardsInFlag);
                }
            } else {
                return -1;
            }
        } else if (cardsInFlag.size == 1) {
            if (deck.getCards().size > 1) {
                if (checkIfSmallestCard(deck, cardsInFlag)) {
                    maxSkirmish = ((cardsInFlag.get(0).getNumber() + 2) * 3) - 3;
                } else if (checkIfMiddleCard(deck, cardsInFlag)) {
                    maxSkirmish = ((cardsInFlag.get(0).getNumber() + 1) * 3) - 3;
                } else if (checkIfBiggestCard(deck, cardsInFlag)) {
                    maxSkirmish = ((cardsInFlag.get(0).getNumber()) * 3) - 3;
                } else {
                    return -1;
                }
            } else {
                return -1;
            }
        } else {
            if (deck.isBigEnough()) {
                maxSkirmish = deck.getHighestThreeConsecutiveCards();
            } else {
                return -1;
            }
            if (maxSkirmish == -1) {
                return -1;
            }
        }
        if (maxSkirmish == -1)
            return maxSkirmish;
        return maxSkirmish + 1000;
    }

    private static boolean checkIfBiggestCard(Deck deck, Array<Card> cardsInFlag) {
        return deck.existsCardWithSpecificNumber(cardsInFlag.get(0).getNumber() - 1)
                && deck.existsCardWithSpecificNumber(cardsInFlag.get(0).getNumber() - 2);
    }

    private static boolean checkIfMiddleCard(Deck deck, Array<Card> cardsInFlag) {
        return deck.existsCardWithSpecificNumber(cardsInFlag.get(0).getNumber() + 1)
                && deck.existsCardWithSpecificNumber(cardsInFlag.get(0).getNumber() - 1);
    }

    private static boolean checkIfSmallestCard(Deck deck, Array<Card> cardsInFlag) {
        return deck.existsCardWithSpecificNumber(cardsInFlag.get(0).getNumber() + 1)
                && deck.existsCardWithSpecificNumber(cardsInFlag.get(0).getNumber() + 2);
    }

    private static int searchExtremeCard(Deck deck, Array<Card> cardsInFlag) {
        int maxSkirmish;
        if (deck.existsCardWithSpecificNumber(cardsInFlag.get(0).getNumber() + 1)) {
            maxSkirmish = ((cardsInFlag.get(0).getNumber() + 1) * 3) - 3;
        } else {
            if (deck.existsCardWithSpecificNumber(cardsInFlag.get(1).getNumber() - 1)) {
                maxSkirmish = ((cardsInFlag.get(0).getNumber()) * 3) - 3;
            } else {
                maxSkirmish = -1;
            }
        }
        return maxSkirmish;
    }

    private static int searchMiddleCard(Deck deck, Array<Card> cardsInFlag) {
        if (deck.existsCardWithSpecificNumber(cardsInFlag.get(0).getNumber() - 1)) {
            return ((cardsInFlag.get(0).getNumber()) * 3) - 3;
        } else {
            return -1;
        }
    }

    private static int getMaxPossibleHost(Deck deck, Array<Card> cardsInFlag) {
        int maxHost;
        if (cardsInFlag.size == 0) {
            maxHost = deck.getThreeBiggestNumbersToPlay();
        } else if (cardsInFlag.size == 1) {
            maxHost = deck.getTwoBiggestNumbersToPlay() + cardsInFlag.get(0).getNumber();
        } else {
            maxHost = deck.getBiggestNumberToPlay() + cardsInFlag.get(0).getNumber()
                    + cardsInFlag.get(1).getNumber();
        }
        return maxHost;
    }

    private static boolean isWedgePossible(Array<Card> cardsInFlag) {
        if (cardsInFlag.size == 0 || cardsInFlag.size == 1) {
            return true;
        } else {
            boolean isSkirmishPossible = isSkirmishPossible(cardsInFlag);
            boolean isBattalionPossible = isBattalionPossible(cardsInFlag);
            return isBattalionPossible && isSkirmishPossible;
        }
    }

    private static boolean isPhalanxPossible(Array<Card> cardsInFlag) {
        if (cardsInFlag.size == 0 || cardsInFlag.size == 1) {
            return true;
        } else {
            return cardsInFlag.get(0).getNumber() == cardsInFlag.get(1).getNumber();
        }
    }

    private static boolean isBattalionPossible(Array<Card> cardsInFlag) {
        if (cardsInFlag.size == 0 || cardsInFlag.size == 1) {
            return true;
        } else {
            return cardsInFlag.get(0).getColor() == cardsInFlag.get(1).getColor();
        }
    }

    private static boolean isSkirmishPossible(Array<Card> cardsInFlag) {

        if (cardsInFlag.size == 0 || cardsInFlag.size == 1) {
            return true;
        } else {
            return cardsInFlag.get(0).getNumber() == cardsInFlag.get(1).getNumber() + 1
                    || cardsInFlag.get(0).getNumber() == cardsInFlag.get(1).getNumber() + 2;
        }
    }

    public static int getBiggestIndex(double[] rewards) {
        int maxRewardIndex = 0;
        double maxRewardValue = -10000;
        for (int i = 0; i < rewards.length; i++) {
            maxRewardIndex = rewards[i] > rewards[maxRewardIndex] ? i : maxRewardIndex;
            maxRewardValue = rewards[i] > maxRewardValue ? rewards[maxRewardIndex] : maxRewardValue;
        }
        List<Integer> idxs = new ArrayList<>();
        for (int i = 0; i < rewards.length; i++) {
            if (rewards[i] == maxRewardValue) {
                idxs.add(i);
            }
        }
        Random random = new Random();
        return idxs.get(random.nextInt(idxs.size()));
    }

    public static ArrayList<GameTurn> getRandomTurns(int number, ArrayList<GameTurn> turns) {
        Random rand = new Random();
        ArrayList<GameTurn> randomList = new ArrayList<>();
        for (int i = 0; i < number; i++) {
            int randomIndex = rand.nextInt(turns.size());
            randomList.add(turns.get(randomIndex));
            turns.remove(randomIndex);
        }
        return randomList;
    }

    public static String getAverageScore(ArrayList<Score> scores, int player) {
        double sum = 0;
        int lostgames=0;
        if (player == 1) {
            for (Score score : scores) {
                if(score.getPlayer1score()>0) {
                    sum += score.getPlayer1score();
                }
                else{
                    lostgames++;
                }
            }
        } else {
            for (Score score : scores) {
                if(score.getPlayer2score()>0) {
                    sum += score.getPlayer2score();
                }
                else{
                    lostgames++;
                }
            }
        }
        sum = sum;
        DecimalFormat df = new DecimalFormat("##");
        df.setRoundingMode(RoundingMode.CEILING);
        return df.format(sum / (scores.size()-lostgames));
    }


    public static String getAverageReward(ArrayList<Score> scores) {
        double sum = 0;
        for (Score score : scores) {
            sum += score.getTotalReward();
        }

        DecimalFormat df = new DecimalFormat("#.##");
        df.setRoundingMode(RoundingMode.CEILING);
        return df.format(sum / scores.size());
    }

    public static int[] getIdxsFromCardId(int cardId) {
        int[] idxs = new int[FLAGS];
        for (int i = 0; i < FLAGS; i++) {
            idxs[i] = cardId + (i * DECK_SIZE);
        }
        return idxs;
    }

    public static Node findBestNodeWithUCT(Node node) {
        int parentVisit = node.getState().getVisitCount();
        List<Node> childNodes = node.getChildArray();
        int maxScoreIndex = 0;
        double maxScoreValue = -10000;
        for (int i = 0; i < childNodes.size(); i++) {
            double score = uctValue(parentVisit, childNodes.get(i).getState().winScore, childNodes.get(i).getState().visitCount);
            maxScoreIndex = score > maxScoreValue ? i : maxScoreIndex;
            maxScoreValue = score > maxScoreValue ? score : maxScoreValue;
        }
        List<Integer> idxs = new ArrayList<>();
        for (int i = 0; i < childNodes.size(); i++) {
            double score = uctValue(parentVisit, childNodes.get(i).getState().winScore, childNodes.get(i).getState().visitCount);
            if (score == maxScoreValue) {
                idxs.add(i);
            }
        }
        Random random = new Random();
        return childNodes.get(idxs.get(random.nextInt(idxs.size())));
    }

    public static double uctValue(int totalParentVisit, double nodeWinScore, int nodeVisit) {
        if (nodeVisit == 0) {
            return Integer.MAX_VALUE;
        }
        return (nodeWinScore / (double) nodeVisit)
                + 1.41 * Math.sqrt(Math.log(totalParentVisit) / (double) nodeVisit);
    }

    public static Node findNodeWithHighestScore(List<Node> childNodes) {
        int maxScoreIndex = 0;
        double maxScoreValue = -10000;
        for (int i = 0; i < childNodes.size(); i++) {
            maxScoreIndex = childNodes.get(i).getState().getWinScore() > maxScoreValue ? i : maxScoreIndex;
            maxScoreValue = childNodes.get(i).getState().getWinScore() > maxScoreValue ? childNodes.get(i).getState().getWinScore() : maxScoreValue;
        }
        List<Integer> idxs = new ArrayList<>();
        for (int i = 0; i < childNodes.size(); i++) {
            if (childNodes.get(i).getState().getWinScore() == maxScoreValue) {
                idxs.add(i);
            }
        }
        Random random = new Random();
        return childNodes.get(idxs.get(random.nextInt(idxs.size())));
    }

    public static Array<Card> getDeepCopyArrayCard (Array<Card> tocopy){
        Array<Card> copy=new Array<Card>();
        for (Card c:tocopy) {
                copy.add(new Card(c));

        }
    return copy;
    }


}
