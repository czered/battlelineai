package de.zoneofcontrol.battleline.game;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import com.badlogic.gdx.utils.Array;

import static de.zoneofcontrol.battleline.resources.Constants.*;

public class Deck {

    private Array<Card> cards = new Array<>();

    public Array<Card> getCards() {
        return cards;
    }

    Deck() {
        populateDeck();
    }

    Deck(Array<Card> newCards) {
        cards = new Array<>(Arrays.copyOf(newCards.toArray(Card.class), newCards.size, Card[].class));
    }

    Deck(Deck deck) {
        cards = new Array<>(Arrays.copyOf(deck.getCards().toArray(Card.class), deck.getCards().size, Card[].class));
    }

    private void populateDeck() {
        int cardID = 0;
        for (int i = 1; i <= CARD_NUMBERS; i++) {
            for (int j = 0; j < COLORS; j++) {
                Card c = new Card(j, i, cardID);
                cardID++;
                cards.add(c);
            }
        }
        if (FIXED_DECK)
            fixedShuffle();
        else
            cards.shuffle();
    }

    private void fixedShuffle() {
        int[] randomlist = new int[]{13, 50, 37, 55, 12, 23, 0, 42, 56, 48, 57, 25, 59, 58, 34, 20, 8, 1, 44, 31, 6,
                51, 24, 39, 11, 30, 46, 7, 28, 2, 49, 40, 19, 27, 32, 54, 36, 14, 52, 16, 41, 29, 22, 33, 47, 15, 35,
                53, 4, 45, 17, 5, 21, 38, 26, 9, 10, 3, 43, 18};

        Array<Card> cardshuffled = new Array<>();
        for (int i = 0; i < randomlist.length; i++) {
            cardshuffled.add(cards.get(randomlist[i]));
        }
        cards = cardshuffled;
    }

    Card getSingleCard() {
        return cards.pop();
    }

    Card getFirstCard() {
        Card c = cards.first();
        cards.removeValue(c, true);
        return c;
    }

    boolean isEmpty() {
        return cards.isEmpty();
    }

    boolean isBigEnough() {
        return cards.size > 2;
    }

    void resetDeck() {
        cards.clear();
        populateDeck();
    }

    Array<Card> getSingleColorCards(int color) {
        List<Card> filtered = Arrays.stream(cards.toArray()).filter(card -> card.getColor() == color)
                .collect(Collectors.toList());
        Card[] newDeck = filtered.toArray(new Card[filtered.size()]);
        return new Array<Card>(newDeck);
    }

    Array<Card> getSingleNumberCards(int number) {
        List<Card> filtered = Arrays.stream(cards.toArray()).filter(card -> card.getNumber() == number)
                .collect(Collectors.toList());
        Card[] newDeck = filtered.toArray(new Card[filtered.size()]);
        return new Array<Card>(newDeck);
    }

    boolean existsCardWithSpecificNumber(int number) {
        Array<Card> filtered = getSingleNumberCards(number);
        return filtered.size != 0;
    }

    void sort() {
        this.cards.sort();
    }

    int getBiggestNumberToPlay() {
        cards.sort();
        int max = cards.get(0).getNumber();
        cards.shuffle();
        return max;
    }

    int getTwoBiggestNumbersToPlay() {
        if (cards.size == 1) {
            return getBiggestNumberToPlay();
        }
        cards.sort();
        int max1 = cards.get(0).getNumber();
        int max2 = cards.get(1).getNumber();
        cards.shuffle();
        return max1 + max2;
    }

    int getThreeBiggestNumbersToPlay() {
        if (cards.size == 1) {
            return getBiggestNumberToPlay();
        }
        if (cards.size == 2) {
            return getTwoBiggestNumbersToPlay();
        }
        cards.sort();
        int max1 = cards.get(0).getNumber();
        int max2 = cards.get(1).getNumber();
        int max3 = cards.get(2).getNumber();
        cards.shuffle();
        return max1 + max2 + max3;
    }

    int getHighestThreeConsecutiveCards() {
        cards.sort();
        int highest = cards.first().getNumber();
        while (highest >= 3) {
            if (existsCardWithSpecificNumber(highest - 1)) {
                if (existsCardWithSpecificNumber(highest - 2)) {
                    cards.shuffle();
                    return (highest * 3) - 3;
                } else {
                    highest = highest - 3;
                }
            } else {
                highest = highest - 2;
            }
        }
        cards.shuffle();
        return -1;
    }

    int getDeckSize() {
        return cards.size;
    }

    void addCards(Array<Card> moreCards) {
        cards.addAll(moreCards);
    }

    public static boolean isFixeddeck() {
        return FIXED_DECK;
    }

}
