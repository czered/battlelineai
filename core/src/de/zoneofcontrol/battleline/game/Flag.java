package de.zoneofcontrol.battleline.game;

import com.badlogic.gdx.utils.Array;
import de.zoneofcontrol.battleline.game.MainGame.PLAYERS;

import java.util.Arrays;

public class Flag {


    enum TIER {
        HOST(0), SKIRMISH(1), BATTALION(2), PHALANX(3), WEDGE(4);
        private final int rank;

        TIER(int rank) {
            this.rank = rank;
        }
    }

    private Array<Card> player1cards;
    private Array<Card> player2cards;

    //The State of the card, is it already claimed and who is the winner
    private int winner = 0;

    //The scoring of the card, each player has tier
    private TIER p1tier;
    private TIER p2tier;
    private int p1score = -1;
    private int p2score = -1;

    Flag() {
        player1cards = new Array<Card>();
        player2cards = new Array<Card>();
    }



    Flag(Array<Card> cards1, Array<Card> cards2, int _winner) {
        this.player1cards = new Array<>(Arrays.copyOf(cards1.toArray(Card.class), cards1.size, Card[].class));
        this.player2cards = new Array<>(Arrays.copyOf(cards2.toArray(Card.class), cards2.size, Card[].class));
        if (this.player1cards.size == 3) {
            checkScore(this.player1cards, PLAYERS.PLAYER1);
        }
        if (this.player2cards .size == 3) {
            checkScore(this.player2cards , PLAYERS.PLAYER2);
        }
        winner = _winner;
    }
    public Flag(Flag f) {
        Array<Card> cards1=f.getPlayer1cards();
        Array<Card> cards2=f.getPlayer2cards();
        int _winner=f.winner;
        this.player1cards = new Array<>(Arrays.copyOf(cards1.toArray(Card.class), cards1.size, Card[].class));
        this.player2cards = new Array<>(Arrays.copyOf(cards2.toArray(Card.class), cards2.size, Card[].class));
        if (this.player1cards.size == 3) {
            checkScore(this.player1cards, PLAYERS.PLAYER1);
        }
        if (this.player2cards .size == 3) {
            checkScore(this.player2cards , PLAYERS.PLAYER2);
        }
        winner = _winner;
    }



    boolean isPlayable(PLAYERS player) {
        if (winner != 0) {
            return false;
        }
        if (player == PLAYERS.PLAYER1) {
            return player1cards.size < 3;
        } else {
            return player2cards.size < 3;
        }
    }

    int playCard(Card c, PLAYERS player) {
        switch (player) {
            case PLAYER1:
                insertCard(c, player1cards, player);
                break;
            case PLAYER2:
                insertCard(c, player2cards, player);
                break;
        }
        return checkWinner();
    }

    void fakeplayCard(Card c, PLAYERS player) {
        switch (player) {
            case PLAYER1:
                insertCard(c, player1cards, player);
                break;
            case PLAYER2:
                insertCard(c, player2cards, player);
                break;
        }
    }


    //insert a Card into the pile of cards and sort it immediately according to number
    private void insertCard(Card c, Array<Card> flagCards, PLAYERS player) {
        if (flagCards.size == 0) {
            flagCards.add(c);
            return;
        }
        int index = 0;
        for (Card card : flagCards) {
            if (card.getNumber() > c.getNumber()) {
                index++;
            }
        }
        flagCards.insert(index, c);
        if (flagCards.size == 3) {
            checkScore(flagCards, player);
        }
    }

    public void checkScore(Array<Card> playerCards, PLAYERS player) {
        TIER tier = determineTier(playerCards);
        int score = determineScore(playerCards, tier);
        switch (player) {
            case PLAYER1:
                p1tier = tier;
                p1score = score;
                break;
            case PLAYER2:
                p2tier = tier;
                p2score = score;
                break;
        }
    }

    private int checkWinner() {
        if (player1cards.size == 3 && player2cards.size == 3) {
            if (p1tier.rank > p2tier.rank) {
                winner = 1;
            } else if (p2tier.rank > p1tier.rank) {
                winner = 2;
            } else if (p1score > p2score) {
                winner = 1;
            } else {
                winner = 2;
            }
        }
        return winner;
    }


    private TIER determineTier(Array<Card> playerCards) {
        if (isFlush(playerCards) && isStraight(playerCards)) {
            return TIER.WEDGE;
        }
        if (isTriple(playerCards)) {
            return TIER.PHALANX;
        }
        if (isFlush(playerCards)) {
            return TIER.BATTALION;
        }
        if (isStraight(playerCards)) {
            return TIER.SKIRMISH;
        }
        return TIER.HOST;
    }

    private int determineScore(Array<Card> deck, TIER tier) {
        int score = 0;
        int tierScore = getTierScore(tier);
        for (Card card : deck) {
            score += card.getNumber();
        }
        return score + tierScore;
    }

    private int getTierScore(TIER tier) {
        switch (tier) {
            case WEDGE:
                return 4000;
            case PHALANX:
                return 3000;
            case BATTALION:
                return 2000;
            case SKIRMISH:
                return 1000;
            case HOST:
            default:
                return 0;

        }
    }

    private boolean isTriple(Array<Card> deck) {
        int num = deck.get(0).getNumber();
        for (int i = 1; i < deck.size; i++) {
            if (deck.get(i).getNumber() != num) {
                return false;
            }
        }
        return true;
    }

    private boolean isFlush(Array<Card> playerCards) {
        int num = playerCards.get(0).getColor();
        for (int i = 1; i < playerCards.size; i++) {
            if (playerCards.get(i).getColor() != num) {
                return false;
            }
        }
        return true;
    }

    private boolean isStraight(Array<Card> cards) {
        int a = cards.get(0).getNumber();
        int b = cards.get(1).getNumber();
        int c = cards.get(2).getNumber();
        if (a == b + 1) {
            return b == c + 1;
        }
        return false;
    }

    int getWinner() {
        return winner;
    }

    void setWinner(int winner) {
        this.winner = winner;
    }

    int getP1score() {
        return p1score;
    }

    int getP2score() {
        return p2score;
    }

    Array<Card> getPlayer1cards() {
        return player1cards;
    }

    Array<Card> getPlayer2cards() {
        return player2cards;
    }

    String getVisualization() {
        String leftside = "";
        String center = " :X: ";
        String rightside = "";
        switch (winner) {
            case 0:
                center = "  :  ";
                break;
            case 1:
                center = " <<  ";
                break;
            case 2:
                center = "  >> ";
                break;
        }
        for (Card c : player1cards) {
            leftside += c.toString();

        }
        for (Card c : player2cards) {
            rightside += c.toString();
        }

        return leftside + center + rightside;
    }
}

