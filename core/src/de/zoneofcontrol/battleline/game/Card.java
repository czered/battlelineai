package de.zoneofcontrol.battleline.game;

import org.apache.commons.lang3.ArrayUtils;

public class Card implements Comparable<Card> {
    private int color;
    private int number;
    private int id;
    private double[] binaryRepresentation;

    Card(int color, int number, int id) {
        this.color = color;
        this.number = number;
        this.id = id;
        this.binaryRepresentation = setUp16BinaryRepresentation();
    }

    Card(Card c){
        this.color=c.color;
        this.number=c.number;
        this.id=c.id;
        this.binaryRepresentation=c.binaryRepresentation;
    }

    int getColor() {
        return color;
    }

    int getNumber() {
        return number;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    double[] getBinaryRepresentation() {
        return binaryRepresentation;
    }

    @Override
    public int compareTo(Card card) {
        return card.number - this.number;
    }

    private double[] setUp16BinaryRepresentation() {
        double[] binaryColor = Utils.getBinaryColor(this.color);
        double[] binaryNumber = Utils.getBinaryNumber(this.number);
        return ArrayUtils.addAll(binaryNumber, binaryColor);

    }

    public String toString() {
        String scolor = "";
        switch (color) {
            case 0:
                scolor = "A";
                break;
            case 1:
                scolor = "B";
                break;
            case 2:
                scolor = "C";
                break;
            case 3:
                scolor = "D";
                break;
            case 4:
                scolor = "E";
                break;
            case 5:
                scolor = "F";
                break;
            case 6:
                scolor = "G";
                break;
        }

        String text = scolor + number;
        return text;
    }
}
