package de.zoneofcontrol.battleline.ai;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import org.deeplearning4j.api.storage.StatsStorage;
import org.deeplearning4j.ui.api.UIServer;
import org.deeplearning4j.ui.stats.StatsListener;
import org.deeplearning4j.ui.storage.InMemoryStatsStorage;

import de.zoneofcontrol.battleline.game.GameTurn;
import de.zoneofcontrol.battleline.game.MainGame;
import de.zoneofcontrol.battleline.game.Score;
import de.zoneofcontrol.battleline.game.Utils;

import static de.zoneofcontrol.battleline.resources.Constants.*;

public class GameTrainer {

    private static Agent playingAgent;
    private static Agent targetAgent;
    private static Agent opponentAgent;
    private static ArrayList<GameTurn> historicalMoves = new ArrayList<>();
    private static ArrayList<GameTurn> gameMoves = new ArrayList<>();
    private static ArrayList<Score> batchScores = new ArrayList<>();
    private static ArrayList<Score> historicalScores = new ArrayList<>();

    private static MainGame game;
    private static int player1wins = 0;
    private static int totalplayer1wins = 0;
    private static int generation = 0;
    private static int numberofevaluations = 0;
    private static int consecutivewins = 0;
    private static long timelastEvaluation = 0;

    private static void doTraining() {
        numberofevaluations = 0;
        consecutivewins = 0;
        Date date = new Date();
        SimpleDateFormat formatter = new SimpleDateFormat("ddMMyyyy-HHmmss");
        String directory = "agent-" + formatter.format(date);
        File dir = new File(directory);
        float maxwinpercentage = 0;
        dir.mkdir();
        String saveplayAgent = directory + "/" + "playingAgentNN.zip";
        String saveLatestAgent = "latest/playingAgentNN.zip";
        String saveLatestLog = "latest/log.csv";
        String savelog = directory + "/" + directory + "-log.csv";
        File locationToSaveLog = new File(savelog);
        File locationToSavePlaying = new File(saveplayAgent);
        File locationToSaveLatest = new File(saveLatestAgent);
        File locationToSaveLatestLog= new File(saveLatestLog);
        startLog(locationToSaveLog, formatter.format(date));
        startNetworkUI();
        startConsoleLog();
        long startTime = System.currentTimeMillis();
        timelastEvaluation = startTime;
        historicalScores = new ArrayList<>();
        for (int i = 0; i < NUM_EPOCHS; i++) {
            historicalScores.addAll(batchScores);
            batchScores = new ArrayList<>();
            trainOneEpoch();
            checkAgentSync(i);
            evaluatePerformance(locationToSaveLog, locationToSavePlaying, locationToSaveLatest,locationToSaveLatestLog,i);

        }
        maxwinpercentage *= 100;
        finishLog(locationToSaveLog, (System.currentTimeMillis() - startTime) / 1000, (int) maxwinpercentage,
                numberofevaluations);
        System.out.println(
                String.format("Time of execution was %2d seconds", (System.currentTimeMillis() - startTime) / 1000));
    }


    private static void trainOneEpoch() {
        for (int j = 0; j < GAMES_PER_EPOCHS; j++) {
            game.createNewGame(playingAgent.getPlayer(), HANDSIZE_TRAINING);
            playingAgent.setEvaluation(false);
            targetAgent.setEvaluation(false);
            gameMoves = new ArrayList<>();
            trainSingleGame();
            if (historicalMoves.size() > BATCH_SIZE) {
                trainHistoricalMoves();
            }
            playingAgent.updateEpsilon();
        }
    }

    private static void evaluatePerformance(File locationToSaveLog, File locationToSaveBest, File locationToSaveLatest, File locationToSaveLatestLog, int i) {
        int winperc = -1;
        float winratio = -1;
        if (i % EVALUATION_INTERVALL == 0) {
            batchScores = new ArrayList<>();
            numberofevaluations++;
            player1wins = 0;
            for (int k = 0; k < EVALUATION_SIZE; k++) {
                playingAgent.setEvaluation(true);
                targetAgent.setEvaluation(true);
                game.createNewGame(playingAgent.getPlayer(), HANDSIZE);
                gameMoves = new ArrayList<>();
                evaluateSingleGame();
                winratio = (float) player1wins / (k + 1);
                if (stopEvalEarly(player1wins, k + 1)) {
                    k = EVALUATION_SIZE;
                }
            }

            winperc = (int) (winratio * 100);
            printConsoleLog(timelastEvaluation, i, winperc);
            timelastEvaluation = System.currentTimeMillis();
            logData(locationToSaveLog, i);
            logData(locationToSaveLatestLog, i);
            checkToUpdateOpponent(winratio);
            try {
                playingAgent.getNet().save(locationToSaveBest, true);
                playingAgent.getNet().save(locationToSaveLatest, true);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private static void checkToUpdateOpponent(float winpercentage) {
        if (winpercentage >= WIN_PERC_NEEDED) {
            consecutivewins++;
            if (consecutivewins >= CONS_WINS_NEEDED) {
                opponentAgent.setNet(playingAgent.getNet().clone());
                consecutivewins = 0;
                generation++;
                playingAgent.increaseEpsilon();
            }

        } else {
            consecutivewins = 0;
        }
    }

    private static boolean stopEvalEarly(int gameswonsofar, int gamesplayed) {
        int gamestowin = (int) (WIN_PERC_NEEDED * EVALUATION_SIZE);
        int remaininggames = EVALUATION_SIZE - gamesplayed;
        float currentprecision = (float) gamesplayed / (float) EVALUATION_SIZE;
        float winratio = ((float) gameswonsofar / (float) gamesplayed);
        float expectedremainingwins = (winratio / currentprecision) * (float) remaininggames;
        if (gamesplayed > EVALUATION_SIZE * PREDICTION_START_PERC) {
            if (expectedremainingwins + gameswonsofar <= gamestowin) {
                if (gameswonsofar < gamestowin) {
                    return true;
                }
            }
        }
        return false;
    }

    private static void checkAgentSync(int i) {
        if (i % AGENT_SYNC == 0) {
            playingAgent.setNet(targetAgent.getNet().clone());
        }
    }

    private static void startConsoleLog() {
        System.out.println("==Starting Training==");
        System.out.println("Epochs: " + NUM_EPOCHS);
        System.out.println("Games per Epoch: " + GAMES_PER_EPOCHS);
        System.out.println("History size: " + BATCH_SIZE);
        System.out.println("Syncing agents : " + AGENT_SYNC);
        System.out.println("=====================");
        System.out.format("%6s %7s %6s %6s %6s %6s %6s %6s", "EPOCH", "AVGREW", "SCR1", "SCR2", "EPSI", "TIME",
                "Q-PRE", "GEN");
        System.out.println();
    }

    private static void printConsoleLog(long timelastEvaluation, int i, int winperc) {
        DecimalFormat df = new DecimalFormat("#.##", DecimalFormatSymbols.getInstance(Locale.GERMANY));
        int seconds = (int) (System.currentTimeMillis() - timelastEvaluation) / 1000;
        System.out.format("%6s %7s %6s %6s %6s %6s %6s %6s %6s", i, Utils.getAverageReward(batchScores), Utils.getAverageScore(batchScores, 1), Utils.getAverageScore(batchScores, 2),
                df.format(playingAgent.EPSILON), seconds,playingAgent.getQprecision(), generation, "|");
        int updatepos = (int) (WIN_PERC_NEEDED * 100);
        int cursorpos = 0;
        for (int j = 0; j < winperc; j++) {
            cursorpos++;
            if (cursorpos == updatepos) {
                System.out.print(":");
            }
            System.out.print("#");
        }
        for (int j = 0; j < 100 - winperc; j++) {
            cursorpos++;
            if (cursorpos == updatepos) {
                System.out.print(":");
            }
            System.out.print(" ");
        }
        System.out.print("|" + winperc + "%");
        System.out.println();
    }

    private static void startLog(File locationToSaveLog, String date) {
        FileWriter csvWriter;
        int evalgames = NUM_EPOCHS / AGENT_SYNC;
        evalgames *= EVALUATION_SIZE;
        try {
            csvWriter = new FileWriter(locationToSaveLog);
            csvWriter.append("========Game Infos:========\n");
            csvWriter.append("Fixed Deck: ;;" + game.getDeck().isFixeddeck() + "\n");
            csvWriter.append("Agent ID: ;;" + date + "\n");
            csvWriter.append("======Train Settings======\n");
            csvWriter.append("Epochs: ;;" + NUM_EPOCHS + "\n");
            csvWriter.append("Games per Epoch: ;;" + GAMES_PER_EPOCHS + "\n");
            csvWriter.append("History size: ;;" + MAX_SIZE + "\n");
            csvWriter.append("History recalls: ;;" + BATCH_SIZE + "\n");
            csvWriter.append("Syncing agents : ;;" + AGENT_SYNC + "\n");
            csvWriter.append("Evaluation Size : ;;" + EVALUATION_SIZE + "\n");
            csvWriter.append("Total Training Games : ;;" + TOTALTRAININGGAMES + "\n");
            csvWriter.append("Total Evaluation Games: ;; " + evalgames + "\n");
            csvWriter.append("======Rewards======\n");
            csvWriter.append("Reward Flag Lost: ;;" + FLAG_LOST_REWARD + "\n");
            csvWriter.append("Reward Flag Won: ;;" + FLAG_WON_REWARD + "\n");
            csvWriter.append("Reward Game Lost: ;;" + GAME_LOST_REWARD + "\n");
            csvWriter.append("Reward Game Won: ;; " + GAME_WON_REWARD + "\n");
            csvWriter.append("========Agent Info========\n");
            csvWriter.append("Network Size (I/H/O): ;;" + NUMBER_INPUTS + "/" + HIDDEN_NODES + "/" + OUTPUTS + "\n");
            csvWriter.append("Learning Rate: ;;" + LEARNING_RATE + "\n");
            csvWriter.append("Initial Weights: ;;" + INITIALWEIGHT + "\n");
            csvWriter.append("Inner Activation : ;;" + INNERACTIVATION + "\n");
            csvWriter.append("Output Activation : ;;" + FINALACTIVATION + "\n");
            csvWriter.append("Loss Function : ;;" + LOSSFUNCTION + "\n");
            csvWriter.append("Epsilon Decay : ;;" + EPSILON_DECAY + "\n");
            csvWriter.append("Minimum Epsilon: ;;" + MIN_EPSILON + "\n");
            csvWriter.append("Additional Notes: ;;" + COMMENTS + "\n");
            csvWriter.append("======================" + "\n");
            csvWriter.append("\n");
            csvWriter.append("\n");
            csvWriter.append("======STARTING TRAINING======" + "\n");
            csvWriter.append("EPOCH;AVGWIN;AVG-SCR;AVG-REW;EPSILON" + "\n");
            csvWriter.flush();
            csvWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void logData(File locationToSaveLog, int epoch) {
        String data = String.format("%d;%f;%s;%s;%f", epoch, (double) player1wins / EVALUATION_SIZE,
                Utils.getAverageScore(batchScores, 1), Utils.getAverageReward(batchScores), playingAgent.EPSILON);
        try {
            FileWriter csvWriter;
            csvWriter = new FileWriter(locationToSaveLog, true);
            csvWriter.append(data + "\n");
            csvWriter.flush();
            csvWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void finishLog(File locationToSaveLog, float seconds, int maxwinperc, int numeval) {
        int evalgames = EVALUATION_SIZE * numeval;
        float averagewinsf = ((float) totalplayer1wins / (float) evalgames) * 100f;
        int averagewins = (int) averagewinsf;
        float timepergame = seconds / (evalgames + TOTALTRAININGGAMES);
        try {
            FileWriter csvWriter;
            csvWriter = new FileWriter(locationToSaveLog, true);
            csvWriter.append("\n");
            csvWriter.append("========FINISHED========\n");
            csvWriter.append("Training time: ;" + (int) seconds + ";seconds\n");
            csvWriter.append("Seconds per game: ;;" + timepergame + "\n");
            csvWriter.append("Average Win: ;;" + averagewins + "%\n");
            csvWriter.append("Max Win: ;;" + maxwinperc + "%\n");
            csvWriter.append("======================" + "\n");
            csvWriter.flush();
            csvWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void startNetworkUI() {
        // Initialize the user interface backend
        UIServer uiServer = UIServer.getInstance();

        // Configure where the network information (gradients, score vs. time etc) is to
        // be stored. Here: store in memory.
        StatsStorage statsStorage = new InMemoryStatsStorage(); // Alternative: new FileStatsStorage(File), for saving
        // and loading later

        // Attach the StatsStorage instance to the UI: this allows the contents of the
        // StatsStorage to be visualized
        uiServer.attach(statsStorage);

        // Then add the StatsListener to collect this information from the network, as
        // it trains
        targetAgent.getNet().setListeners(new StatsListener(statsStorage));
    }

    private static void trainSingleGame() {
        while (game.getPlayerturnstate() != MainGame.BATTLELINE_STATES.GAME_OVER) {
            if (game.getCurrentplayer() == playingAgent.getPlayer()) {
                game.executeAITurn(playingAgent);
                if (game.didPlayerPass()) {
                    continue;
                }
                gameMoves.add(game.getLastGameTurn());
                checkClaimedFlags(playingAgent.getPlayer());
            } else {
                game.executeAITurn(opponentAgent);
                if (game.didPlayerPass()) {
                    continue;
                }
                MainGame.PLAYERS otherPlayer = playingAgent.getPlayer() == MainGame.PLAYERS.PLAYER1
                        ? MainGame.PLAYERS.PLAYER2
                        : MainGame.PLAYERS.PLAYER1;
                checkClaimedFlags(otherPlayer);
            }
        }
        addRewards();
        historicalMoves.addAll(gameMoves);
        checkHistoricalSize();
        targetAgent.train(gameMoves);
        if (game.getWinner() == 1) {
            player1wins++;
        } else {
        }
        int[] scores = game.getGameBoard().getPlayersScore();
        double totalReward = gameMoves.stream().map(GameTurn::getReward).reduce((double) 0, Double::sum);
        Score score = new Score(scores[0], scores[1], totalReward, gameMoves.size());
        batchScores.add(score);
    }

    private static void evaluateSingleGame() {
        while (game.getPlayerturnstate() != MainGame.BATTLELINE_STATES.GAME_OVER) {
            if (game.getCurrentplayer() == playingAgent.getPlayer()) {
                game.executeAITurn(playingAgent);
                gameMoves.add(game.getLastGameTurn());
                checkClaimedFlags(playingAgent.getPlayer());
            } else {
                game.executeAITurn(opponentAgent);
                MainGame.PLAYERS otherPlayer = playingAgent.getPlayer() == MainGame.PLAYERS.PLAYER1
                        ? MainGame.PLAYERS.PLAYER2
                        : MainGame.PLAYERS.PLAYER1;
                checkClaimedFlags(otherPlayer);
            }
        }
        addRewards();
        if (game.getWinner() == 1) {
            player1wins++;
            totalplayer1wins++;
        } else {
        }
        int[] scores = game.getGameBoard().getPlayersScore();
        double totalReward = gameMoves.stream().map(GameTurn::getReward).reduce((double) 0, Double::sum);
        Score score = new Score(scores[0], scores[1], totalReward, gameMoves.size());
        batchScores.add(score);
       // game.printGameboard();
    }

    private static void trainHistoricalMoves() {
        ArrayList<GameTurn> randomTurns = Utils.getRandomTurns(BATCH_SIZE,
                (ArrayList<GameTurn>) historicalMoves.clone());
        targetAgent.train(randomTurns);
    }

    private static void checkClaimedFlags(MainGame.PLAYERS player) {
        double reward = player != game.getCurrentplayer() ? FLAG_WON_REWARD : FLAG_LOST_REWARD;
        if (game.isFlagClaimed() != -1) {
            int finishedFlag = game.isFlagClaimed();
            setFlagRewards(reward, finishedFlag);
        }
    }

    private static void addRewards() {
        double reward = game.getWinner() == playingAgent.getPlayer().value ? GAME_WON_REWARD : GAME_LOST_REWARD;
        double length = gameMoves.size();
        for (int i = 0; i < length; i++) {
            double proportion = (i + 1) / length;
            gameMoves.get(i).setReward(gameMoves.get(i).getReward() + (reward * proportion));
        }
    }

    private static void setFlagRewards(double reward, int flagSelected) {
        gameMoves.stream().filter(gameTurn -> gameTurn.getFlagPlayed() == flagSelected).forEach(gameTurn -> gameTurn
                .setReward(gameTurn.getReward() + (reward / (3 / gameTurn.getNumberOfCardsInFlag()))));
    }

    private static void checkHistoricalSize() {
        while (historicalMoves.size() > MAX_SIZE) {
            historicalMoves.remove(0);
        }
    }

    public static void main(String[] args) {
        game = new MainGame();
        game.create();
        playingAgent = new Agent(MainGame.PLAYERS.PLAYER1);
        targetAgent = new Agent(MainGame.PLAYERS.PLAYER1);
        opponentAgent = new Agent(MainGame.PLAYERS.PLAYER2);
        opponentAgent.setNet(playingAgent.getNet().clone());
        opponentAgent.setEvaluation(true);
        doTraining();
    }

}
