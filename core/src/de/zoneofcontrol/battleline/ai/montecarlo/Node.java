package de.zoneofcontrol.battleline.ai.montecarlo;

import de.zoneofcontrol.battleline.game.GameState;
import de.zoneofcontrol.battleline.game.MainGame;
import de.zoneofcontrol.battleline.game.Utils;

import java.util.List;
import java.util.Random;

public class Node {
    GameState state;
    Node parent;
    List<Node> childArray;
    MainGame.PLAYERS player;

    public GameState getState() {
        return state;
    }

    public void setState(GameState state) {
        this.state = state;
    }

    public Node getParent() {
        return parent;
    }

    public void setParent(Node parent) {
        this.parent = parent;
    }

    public List<Node> getChildArray() {
        return childArray;
    }

    public void setChildArray(List<Node> childArray) {
        this.childArray = childArray;
    }

    public MainGame.PLAYERS getPlayer() {
        return player;
    }

    public void setPlayer(MainGame.PLAYERS player) {
        this.player = player;
    }

    public Node getRandomChildNode() {
        Random random = new Random();
        return childArray.get(random.nextInt(childArray.size()));
    }

    public Node getChildWithMaxScore() {
        return Utils.findNodeWithHighestScore(childArray);
    }

}
