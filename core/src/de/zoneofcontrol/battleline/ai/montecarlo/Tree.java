package de.zoneofcontrol.battleline.ai.montecarlo;

import java.util.ArrayList;

public class Tree {
    Node root;

    Tree() {
        root = new Node();
        root.setChildArray(new ArrayList<>());
    }

    public Node getRoot() {
        return root;
    }

    public void setRoot(Node root) {
        this.root = root;
    }
}
