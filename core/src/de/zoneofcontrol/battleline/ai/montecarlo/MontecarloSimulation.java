package de.zoneofcontrol.battleline.ai.montecarlo;

import de.zoneofcontrol.battleline.game.GameState;
import de.zoneofcontrol.battleline.game.MainGame;
import de.zoneofcontrol.battleline.game.Utils;

import java.util.ArrayList;
import java.util.List;

public class MontecarloSimulation {

    public static final int NUM_SIMULATIONS = 500;
    static MainGame game = new MainGame();
    static {
        game.create();
    }

    public double getBestOutput(GameState state, int flagsDone) {
        long startTime = System.currentTimeMillis();
        Tree tree = new Tree();
        Node rootNode = tree.getRoot();
        rootNode.setState(state);
        // rootNode.setPlayer(state.getPlayer());
        int num_games_played = 0;
        while (num_games_played < NUM_SIMULATIONS + (flagsDone * 150)) {
            Node promisingNode = selectPromisingNode(rootNode);
//            if (!promisingNode.getState().isFinishedGame()) {
//                expandNode(promisingNode);
//            }
            Node nodeToExplore = promisingNode;
            if (promisingNode.getChildArray().size() > 0) {
                nodeToExplore = promisingNode.getRandomChildNode();
            }
            MainGame.PLAYERS winner = simulateRandomPlayout(nodeToExplore);
            backPropogation(nodeToExplore, winner);
            num_games_played++;
        }
//        System.out.println("It takes " + (System.currentTimeMillis() - startTime) + " to run a total of " + NUM_SIMULATIONS + " simulations.");
//        System.out.println("Node with PLAYER " + rootNode.getPlayer().value + " was " + rootNode.getState().getVisitCount() + " times played and won " + rootNode.getState().getWinScore() + " times.");

        return rootNode.getState().getWinScore();
    }

    private MainGame.PLAYERS simulateRandomPlayout(Node nodeToExplore) {
        game.createNewGameFromNode(nodeToExplore);
        while (game.getPlayerturnstate() != MainGame.BATTLELINE_STATES.GAME_OVER) {
            // game.executeGameRound();
        }
        return game.getWinner() == 1 ? MainGame.PLAYERS.PLAYER1 : MainGame.PLAYERS.PLAYER2;
    }

    private Node selectPromisingNode(Node rootNode) {
        Node node = rootNode;
        while (node.getChildArray().size() != 0) {
            node = Utils.findBestNodeWithUCT(node);
        }
        return node;
    }

    private void expandNode(Node node) {
        MainGame.PLAYERS childPlayer = node.getPlayer() == MainGame.PLAYERS.PLAYER1 ? MainGame.PLAYERS.PLAYER2 : MainGame.PLAYERS.PLAYER1;
        List<GameState> possibleStates = node.getState().getAllPossibleChildStates();
        possibleStates.forEach(state -> {
            Node newNode = new Node();
            newNode.setState(state);
            newNode.setParent(node);
            newNode.setPlayer(childPlayer);
            newNode.setChildArray(new ArrayList<>());
            node.getChildArray().add(newNode);
        });
    }

    private void backPropogation(Node nodeToExplore, MainGame.PLAYERS winner) {
        Node tempNode = nodeToExplore;
        while (tempNode != null) {
            tempNode.getState().incrementVisit();
            if (tempNode.getPlayer() == winner) {
                tempNode.getState().incrementScore();
            }
            tempNode = tempNode.getParent();
        }
    }
}
