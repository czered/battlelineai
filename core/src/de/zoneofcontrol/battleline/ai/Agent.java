package de.zoneofcontrol.battleline.ai;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

import com.badlogic.gdx.utils.Array;
import de.zoneofcontrol.battleline.game.*;
import org.deeplearning4j.datasets.iterator.impl.ListDataSetIterator;
import org.deeplearning4j.nn.conf.NeuralNetConfiguration;
import org.deeplearning4j.nn.conf.layers.DenseLayer;
import org.deeplearning4j.nn.conf.layers.OutputLayer;
import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;
import org.deeplearning4j.nn.weights.WeightInit;
import org.deeplearning4j.util.ModelSerializer;
import org.nd4j.evaluation.regression.RegressionEvaluation;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.dataset.DataSet;
import org.nd4j.linalg.dataset.api.iterator.DataSetIterator;
import org.nd4j.linalg.factory.Nd4j;
import org.nd4j.linalg.learning.config.AdaGrad;
import org.nd4j.linalg.learning.config.Nadam;
import org.nd4j.linalg.learning.config.RmsProp;

import static de.zoneofcontrol.battleline.resources.Constants.*;

public class Agent {

    private Random rdm;
    private int seed;
    private boolean evaluation = false;
    private MainGame.PLAYERS player;
    private MultiLayerNetwork net;
    private double qprecision=0;

    double EPSILON = 0.9999;

    Agent(MainGame.PLAYERS player) {
        rdm = new Random();
        seed = rdm.nextInt();
        this.player = player;
        initNet();
        loadNet();
    }

    private void loadNet() {
        try {
            this.net = ModelSerializer.restoreMultiLayerNetwork("latest/playingAgentNN.zip");
            System.out.println("Net loaded: " + net.summary());
        } catch (IOException e) {
            System.out.println("Failed to load net");
            e.printStackTrace();
        }
    }

    private void initNet() {
        NeuralNetConfiguration.ListBuilder netBuilder = new NeuralNetConfiguration.Builder().biasInit(0).updater(new AdaGrad())
                .seed(seed).list();
        netBuilder.layer(new DenseLayer.Builder().nIn(NUMBER_INPUTS).nOut(HIDDEN_NODES).activation(INNERACTIVATION)
                // random initialize weights with values between 0 and 1
                .weightInit(INITIALWEIGHT).build());
        for (int i = 0; i < NUM_HIDDEN_LAYERS; i++) {
            netBuilder.layer(new DenseLayer.Builder().nIn(HIDDEN_NODES).nOut(HIDDEN_NODES).activation(INNERACTIVATION)
                    // random initialize weights with values between 0 and 1
                    .weightInit(INITIALWEIGHT).build());
        }
        netBuilder.layer(new OutputLayer.Builder(LOSSFUNCTION).nOut(OUTPUTS).activation(FINALACTIVATION)
                .weightInit(WeightInit.ONES).build());
        MultiLayerNetwork net = new MultiLayerNetwork(netBuilder.build());
        net.init();
        this.net = net;
    }

    public MainGame.PLAYERS getPlayer() {
        return player;
    }

    public MultiLayerNetwork getNet() {
        return net;
    }

    public void setNet(MultiLayerNetwork net) {
        this.net = net;
    }

    public void train(ArrayList<GameTurn> turns) {
        List<DataSet> list = new ArrayList<>();


        for (int i = 0; i < turns.size(); i++) {
            GameTurn turn = turns.get(i);
            INDArray input = turn.getMove().getInputs();
            double reward = turn.getAction().getReward();
            double[] targetOutput = net.output(input).toDoubleVector();

            targetOutput[0] = reward;
            INDArray netOutputsForTrain = Nd4j.create(targetOutput, new int[]{1, OUTPUTS});
            DataSet ds = new DataSet(input, netOutputsForTrain);
            list.add(ds);
        }


        DataSetIterator iter = new ListDataSetIterator(list);
        RegressionEvaluation regeval=net.evaluateRegression(iter);
        qprecision=regeval.averageMeanAbsoluteError();
        net.fit(iter);

    }

    public Move getMove(GameState state, int cardsSize, float random, Deck remainingCards, Array<Card> playerHand, int flag) {
        Move move;
        if (random < EPSILON && !evaluation) {
            move = randomAction(state,remainingCards,playerHand,flag);
        } else
            move = maxQAction(state,remainingCards,playerHand,flag);
        return move;
    }

    private Move maxQAction(GameState state,Deck remainingCards, Array<Card> playerHand, int flag) {
        Array<Move> moves=new Array<Move>();
        Array<Card> handcopy=Utils.getDeepCopyArrayCard(playerHand);
        for (Card c: playerHand) {
            INDArray input = state.getSingleFlagInputs(remainingCards,getPlayer(),c,flag,handcopy);
            double[] output = net.output(input).toDoubleVector();
            Move m=new Move(c,flag,output,input);


            if(isFlagPlayable(flag,state))
                moves.add(m);
            }
        return selectBestCardForFlag(moves);
    }

    private Move selectBestCardForFlag(Array<Move> moves) {
        Move move = null;
        if (moves.size > 0) {
            move = moves.random();
            for (Move m : moves) {
                if (m.getQ() > move.getQ()) {
                    move = m;
                }
            }
        }
        return move;
    }


    private Move randomAction(GameState state,Deck remainingCards, Array<Card> playerHand, int flag){
        Array<Move> moves=new Array<Move>();
        Array<Card> handcopy=Utils.getDeepCopyArrayCard(playerHand);
        for (Card c: playerHand) {
            INDArray input = state.getSingleFlagInputs(remainingCards,getPlayer(),c,flag,handcopy);
            double[] output = net.output(input).toDoubleVector();
            Move m=new Move(c,flag,output,input);
            m.setRandom(true);
            if(isFlagPlayable(flag,state))
                moves.add(m);
        }
        return moves.random();
    }




    private boolean isFlagPlayable(int flag, GameState state) {
        for (int j = 0; j < state.getPlayableFlags().length; j++) {
            if (state.getPlayableFlags()[j] == flag)
                return true;
        }
        return false;

    }

    private int[] convertOutputToMove(int outputPosition, GameState state) {
        int flagPosition = 0;
        int[] move = new int[]{-1, -1, -1, -1};
        while (outputPosition >= DECK_SIZE) {
            outputPosition -= DECK_SIZE;
            flagPosition++;
        }
        move[2] = outputPosition;
        move[0] = getCardPositionFromHand(state, outputPosition); // Card Id
        int finalFlagPosition = flagPosition;
        move[1] = Arrays.stream(state.getPlayableFlags()).filter(f -> f == finalFlagPosition).findAny().isPresent() ? finalFlagPosition : -1; // Flag Position
        return move;
    }

    private int getCardPositionFromHand(GameState state, int cardId) {
        for (int i = 0; i < state.getPlayerhand().size; i++) {
            if (cardId == state.getPlayerhand().get(i).getId()) {
                return i;
            }
        }
        return -1;
    }

    public void updateEpsilon() {
        double newEpsilon = EPSILON * EPSILON_DECAY;
        EPSILON = newEpsilon < MIN_EPSILON ? MIN_EPSILON : newEpsilon;
    }

    public void setEvaluation(boolean b) {
        evaluation = b;

    }

    public void increaseEpsilon(){
        EPSILON=EPSILON+(1-EPSILON)*0.5;
    }

    public double getQprecision() {
        return qprecision;
    }
}
