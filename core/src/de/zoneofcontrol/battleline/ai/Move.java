package de.zoneofcontrol.battleline.ai;

import de.zoneofcontrol.battleline.game.Card;
import org.nd4j.linalg.api.ndarray.INDArray;

public class Move {
    int flag;
    double q=-111;
    INDArray inputs;
    double[] outputs;
    Card card;



    boolean random=false;




    public Move(Card card, int flag, double[] outputs, INDArray inputs){
        this.card=card;
        this.flag=flag;
        this.q=outputs[0];
        this.outputs=outputs;
        this.inputs=inputs;
    }

    public Move() {

    }

    public boolean isRandom() {
        return random;
    }

    public void setRandom(boolean random) {
        this.random = random;
    }


    public void setCard(Card card) {
        this.card = card;
    }

    public Card getCard() {
        return card;
    }

    public int getFlag() {
        return flag;
    }

    public void setFlag(int flag) {
        this.flag=flag;
    }

    public double getQ() {
        return q;
    }

    public INDArray getInputs() {
        return inputs;
    }

    public String toString() {
        return "Card: " + card.toString() + " Flag: " + flag + " Q: " + q;
    }

    public void setQ(double q) {
        this.q=q;
    }

    public double[] getOutputs(){
        return outputs;
    }
}
